! Reload at any time with xrdb -merge .Xdefaults
! This file _can_ have ccp-type #ifdefs
! These values are read after the app-defaults files and override them
! NB this doesnt appear to be read by gdm/gnome!!!

*numeric:	C
*timeFormat:	C
*customization:	-color

!!Xft.antialias:  true

!Xft.rgba:       rgb
! ... Xft.rgba: unknown, rgb, bgr, vrgb, vbgr, none
! ... subpixel geometry

!!Xft.hinting:    true

!!Xft.hintstyle:  hintfull
! ... Xft.hintstyle: hint{none,slight,full}:

! from https://wiki.archlinux.org/index.php/Font_Configuration:
Xft.autohint: 0
Xft.hintstyle:  hintslight
Xft.hinting: 1
Xft.antialias: 1
! leave these to .Xresources.$(hostname):
! Xft.lcdfilter:  lcddefault
! Xft.rgba: rgb

BH.Xresources: true

!Emacs.default.attributeFont: -misc-fixed-medium-r-normal--14-*-*-*-*-*-iso8859-1

! good old a14:
! Emacs*font: -misc-fixed-medium-r-normal--14-*-*-*-*-*-iso8859-1
!Emacs*FontBackend: xft

!Emacs*font: Bitstream Vera Sans Mono-9
! Emacs*font: Bitstream Vera Sans Mono-10
! Emacs*font: Liberation Mono-10

! Lets try this one:
! Emacs*font: -xos4-terminus-medium-r-normal--16-140-*-*-*-*-*-*
! Emacs*font: -*-fixed-medium-r-normal-*-20-*-*-*-*-*-*-*
! Emacs*font: -bitstream-bitstream vera sans mono-bold-r-normal-*-15-*-100-100-*-*-*-*

! This is not bad:
!Emacs*font: -jmk-neep-medium-r-normal-*-15-*-*-*-*-*-iso8859-*

! XEmacs:
!Emacs*EmacsFrame.geometry: 89x69+0+0
! GNU Emacs: better handled by -g option - XEMacs barfs on this:
!Emacs*geometry: 87x72+0+0
!Emacs*foreground: white
!Emacs*background: #eaeaea
!Emacs*background: black

! to fix ddd scroll-wheel direction:
*XmText.Translations: #override\n\
	<Btn5Down>,<Btn5Up>: scroll-one-line-up()scroll-one-line-up()scroll-one-line-up()scroll-one-line-up()\n\
	<Btn4Down>,<Btn4Up>: scroll-one-line-down()scroll-one-line-down()scroll-one-line-down()scroll-one-line-down()\n

! For neXtaw:

!*Font: 		-*-lucidatypewriter-*-r-*-*-*-120-*-*-*-*-iso8859-1
!General resources

!*ShapeStyle:	Rectangle
!*Command.ShapeStyle:	Rectangle
!*Toggle.ShapeStyle:	Rectangle
!*ShadowWidth:	2
!*Form*Text*Background:	white
!*Form*Text*Scrollbar*Background: gray
!*Text*Background:	white
!*Text*Foreground:	black
!*Text*ThreeD*Background:	gray
!*Text*Scrollbar*Background:	gray
!*Viewport.background:	gray
!*BeNiceToColormap: 0
!*Toggle.Font:  -adobe-helvetica-medium-r-*-*-12-*-*-*-*-*-*-*
!*Command.Font: -adobe-helvetica-medium-r-*-*-12-*-*-*-*-*-*-*
!*Command.Background:	gray
!*Command.highlightThickness:	0
!*Toggle.highlightThickness:	0
!*Toggle.Background:	gray
!*MenuButton.Background:	gray
!*SimpleMenu.Background: gray
!*List*Background:	gray
!*Box.Background:	gray
!*Label.Background:	gray
!*Label.ShadowWidth:	0
!*Scrollbar.Background:	gray
!*SimpleMenu*MenuLabel*Background: black
!*SimpleMenu*MenuLabel*Font: -adobe-helvetica-bold-r-*-*-12-*-*-*-*-*-*-*
!*SimpleMenu*Font:	-adobe-helvetica-medium-r-*-*-12-*-*-*-*-*-*-*
!*Dialog.Background:	gray
!*Form*Background:	gray
!*Form*Viewport*Background:	white
!*Form*Viewport*Scrollbar*Background:	grey
!*MenuButton.translations:  Any<BtnDown>:  PopupMenu()

!! Some Settings for Applications

! XFontSel
xfontsel*Toggle*ToggleStyle: check
xfontsel*MenuButton*Justify:	left
xfontsel*MenuButton*MenuButtonStyle: select

! xpaint
xpaint*filebrowser*Form*Toggle*ToggleStyle: radio
xpaint*Label*Background: #a6b6cf
xpaint*Command*Background: #a6b6cf
xpaint*filebrowser*form*Toggle*Background: #a6b6cf

Canvas*filebrowser*Form*Toggle*ToggleStyle: radio
Canvas*filebrowser*Label*Background: #a6b6cf
Canvas*filebrowser*Command*Background: #a6b6cf
Canvas*filebrowser*form*Toggle*Background: #a6b6cf

! sced
sced*Toggle*ToggleStyle: check

! xgc
xgc*Toggle*ToggleStyle: radio
xgc*dashlist*ToggleStyle: simple
xgc*planemask*ToggleStyle: simple
xgc*Justify: left
xgc*Scrollbar.DrawArrows: False

! xfig
fig*Scrollbar.shadowWidth:	2
fig*Form*Scrollbar.ShadowWidth:	2
fig*edit_panel*MenuButton*MenuButtonStyle: select
fig*Form*MenuButton*MenuButtonStyle: select

! xedit
xedit*messageWindow*Scrollbar*DrawArrows:	false

! xterm settings
XTerm*Scrollbar.Thickness:	18
XTerm*Scrollbar.DrawBorder:	0
XTerm*font: -misc-fixed-medium-r-normal--14-*-*-*-*-*-iso8859-1
*VT100.Background: white
!*VT100.Background: #eaeaea
*VT100.Foreground: black

!uxrvt settings:

URxvt.font: xft:Monospace-10
URxvt.boldFont: xft:Monospace-10:bold
URxvt.italicFont: xft:Monospace-10:italic
URxvt.boldItalicFont: xft:Monospace-10:bold:italic

! Bind ^0 ^- ^+ and ^\ to small, normal, big and huge fonts respectively.
URxvt.keysym.C-0: command:\033]710;xft:Monospace-9\007\033]711;xft:Monospace-9:bold\007\033]712;xft:Monospace-9:italic\007\033]713;xft:Monospace-9:bold:italic\007
URxvt.keysym.C-minus: command:\033]710;xft:Monospace-10\007\033]711;xft:Monospace-10:bold\007\033]712;xft:Monospace-10:italic\007\033]713;xft:Monospace-10:bold:italic\007
URxvt.keysym.C-equal: command:\033]710;xft:Monospace-16\007\033]711;xft:Monospace-16:bold\007\033]712;xft:Monospace-16:italic\007\033]713;xft:Monospace-16:bold:italic\007
URxvt.keysym.C-backslash: command:\033]710;xft:Monospace-20\007\033]711;xft:Monospace-20:bold\007\033]712;xft:Monospace-20:italic\007\033]713;xft:Monospace-20:bold:italic\007
URxvt*termName: xterm

! rxvt settings
Rxvt*Scrollbar.Thickness:	18
Rxvt*Scrollbar.DrawBorder:	0
!Rxvt*font: a14
!Rxvt*font: -misc-fixed-medium-r-normal-*-20-200-75-75-c-100-iso8859-1

! Not bad: on bhepple-pc - 5 6 & 7 dont work:
!Rxvt*font1: -jmk-neep-medium-r-normal-*-11-*-*-*-*-*-*-*
!Rxvt*font2: -jmk-neep-medium-r-normal-*-13-*-*-*-*-*-*-*
!Rxvt*font: -jmk-neep-medium-r-normal-*-15-*-*-*-*-*-*-*
!Rxvt*font3: -jmk-neep-medium-r-normal-*-24-*-*-*-*-*-*-*
!Rxvt*font4: -xos4-terminus-medium-r-normal-*-32-*-*-*-*-*-*-*

! On raita: (B&H not on Debian systems)
!Rxvt*font1: -b&h-luxi mono-medium-r-normal--12-*-100-100-m-*-iso8859-1
!Rxvt*font2: -b&h-luxi mono-medium-r-normal--14-*-100-100-m-*-iso8859-1
!Rxvt*font: -b&h-luxi mono-medium-r-normal--15-*-100-100-m-*-iso8859-1
!Rxvt*font3: -b&h-luxi mono-medium-r-normal--18-*-100-100-m-*-iso8859-1
!Rxvt*font4: -b&h-luxi mono-medium-r-normal--20-*-100-100-m-*-iso8859-1
!Rxvt*font5: -b&h-luxi mono-medium-r-normal--22-*-100-100-m-*-iso8859-1
!Rxvt*font6: -b&h-luxi mono-medium-r-normal--24-*-100-100-m-*-iso8859-1

! These are the default Rxvt*fontn settings on bhepple-pc (ubuntu):
!Rxvt*font1: 6x10
!Rxvt*font2: 6x13
!Rxvt*font:  7x14
!Rxvt*font3: 8x13
!Rxvt*font4: 9x15

Rxvt*font1: -misc-fixed-medium-r-normal-*-14-*-100-100-c-*-iso8859-1
Rxvt*font2: -misc-fixed-medium-r-normal-*-15-*-100-100-c-*-iso8859-1
Rxvt*font:  -misc-fixed-medium-r-normal-*-18-*-100-100-c-*-iso8859-1
Rxvt*font3: -misc-fixed-medium-r-normal-*-20-*-100-100-c-*-iso8859-1
Rxvt*font4: -jmk-neep-medium-r-normal-*-24-*-*-*-c-*-iso8859-1
Rxvt*font5: -xos4-terminus-medium-r-normal-*-28-*-*-*-c-*-iso8859-1
Rxvt*font6: -xos4-terminus-medium-r-normal-*-32-*-*-*-c-*-iso8859-1

Rxvt*reverseVideo: off
Rxvt*scrollBar: off
Rxvt*backspacekey: 008
Rxvt*foreground: black
!Rxvt*background: #eaeaea
Rxvt*background: white
Rxvt*saveLines: 500
! Rxvt*termName: xterm
! Rxvt*font: a14
Rxvt*cutchars: '`"\'()*,;<=>?@[]{|}:'

! ghostview
!ghostview*MenuButton*Font: -*-helvetica-medium-r-*-*-12-*-*-*-*-*-iso8859-1
!ghostview*MenuButton*Justify: left
!ghostview*MenuButton*MenuButtonStyle: action
!ghostview*titleButton.MenuButtonStyle: simple
!ghostview*dateButton.MenuButtonStyle: simple

! xman
!xman*Label*Font: -adobe-helvetica-bold-r-*-*-12*
!xman*Command*ShapeStyle: rectangle

! xmorph
!xmorph*MenuButton*MenuButtonStyle: action
!xmorph*MenuButton*Justify: left
!xmorph*scrollbar_mesh*DrawArrows: false
!xmorph*scrollbar_image*DrawArrows: false
!xmorph*Scrollbar*AlwaysVisible: true

!chimera
!chimera*Toggle*ToggleStyle: radio
!chimera*Toggle*Background: gray

!----------------------------------------- end of neXtaw

xosview*serial1: True
xosview*net: True
xosview*netBandWidth: 2000000
xosview*netDecay: True
xosview*geometry: 300x350

!AcroRead*geometry: 1228x1024+0+0
!GV*Ghostview*geometry: 1228x1024+0+0

!ROX-Filer*geometry: 500x600

! from http://koala.ilog.fr/anyboard/MouseWheel/posts/1598.html#1611
AcroRead*XmScrollBar.baseTranslations: #augment \
Shift<Btn5Down>: PageDownOrRight(0) \n Shift<Btn4Down>: PageUpOrLeft(0) \n\
Ctrl<Btn5Down>: IncrementDownOrRight(0) IncrementDownOrRight(0) IncrementDownOrRight(0) \n\
Ctrl<Btn4Down>: IncrementUpOrLeft(0) IncrementUpOrLeft(0) IncrementUpOrLeft(0) \n\
<Btn5Down>: IncrementDownOrRight(0) \n <Btn4Down>: IncrementUpOrLeft(0) \n 

!AcroRead*XacroView.translations: #augment \
!Shift<Btn5Down>: PageDownOrRight(0) \n Shift<Btn4Down>: PageUpOrLeft(0) \n\
!Ctrl<Btn5Down>: IncrementDownOrRight(0) IncrementDownOrRight(0) IncrementDownOrRight(0) \n\
!Ctrl<Btn4Down>: IncrementUpOrLeft(0) IncrementUpOrLeft(0) IncrementUpOrLeft(0) \n\
!<Btn5Down>: IncrementDownOrRight(0) \n <Btn4Down>: IncrementUpOrLeft(0) \n 

!## Athena text widgets:
*Paned.Text.translations: #override\n\
	Shift<Btn4Down>,<Btn4Up>: scroll-one-line-down()\n\
	Shift<Btn5Down>,<Btn5Up>: scroll-one-line-up()\n\
	Ctrl<Btn4Down>,<Btn4Up>: previous-page()\n\
	Ctrl<Btn5Down>,<Btn5Up>: next-page()\n\
	None<Btn4Down>,<Btn4Up>: scroll-one-line-down()scroll-one-line-down()scroll-one-line-down()scroll-one-line-down()scroll-one-line-down()\n\
	None<Btn5Down>,<Btn5Up>: scroll-one-line-up()scroll-one-line-up()scroll-one-line-up()scroll-one-line-up()scroll-one-line-up()\n\


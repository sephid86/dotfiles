# Login initialisation for interactive bash

# This _should_ be executed by the window manager startup scripts when
# doing a graphical login, in which case TERM will probably be 'dumb'
# until a graphical term is started. Moral - don't fritz with TERM
# here, do it in .bashrc, which is executed for non-login but
# interactive sessions.

#export BH_VERBOSE="yes"

[[ "$BH_VERBOSE" ]] && {
    echo "`hostname`:.bash_profile: TERM=$TERM"
    echo "PATH=$PATH"
}

[[ "$BH_BASH_PROFILE" ]] && {
    [[ "$BH_VERBOSE" ]] && echo ".bash_profile already visited"
    return
}

# set -x

export BH_BASH_PROFILE="yes"

##############################
# execute a command silently #
##############################
silent() {
    "$@" >/dev/null 2>&1
}

#########################################################################
# Returns true (0) if $1 and $2 are non-null and $1 is an element in $2 #
# usage: in_path /opt/java/lib LD_LIBRARY_PATH                          #
# $1 is a directory name (could contain spaces)                         #
# $2 is the ':' separated list such as PATH LD_LIBRARY_PATH MANPATH etc #
# (could be "" or contain spaces)                                       #
#########################################################################
in_path() {
    CANDIDATE="$1"
    AGGREGATE="$2"
    [[ ":${AGGREGATE}:" == *:"$CANDIDATE":* ]] && return 0
    return 1
}

##################################################################
# Remove duplicate path elements from $1. Path path elements may #
# contain spaces.                                                #
##################################################################
remove_dups() {
    local -a OLD_PATH OLD_PATH_TEMP NEW_PATH
    local OLD_ELEMENT NEW_ELEMENT SAVE_IFS SKIP

    SAVE_IFS="$IFS"
    IFS=":"
    OLD_PATH=( $@ ) # note: not "$@" !!
    IFS="$SAVE_IFS"

    # this alternative to the above 4 lines works as long as there's
    # no spaces in $@:
    # OLD_PATH=( $(IFS=":"; echo $@ ) )

    for OLD_ELEMENT in "${OLD_PATH[@]}"; do
        SKIP=""
        for NEW_ELEMENT in "${NEW_PATH[@]}"; do
            [ "$NEW_ELEMENT" = "$OLD_ELEMENT" ] && SKIP="1" && break
        done
        [ "$SKIP" ] || NEW_PATH=( "${NEW_PATH[@]}" "$OLD_ELEMENT" )
    done

    SKIP=1
    for NEW_ELEMENT in "${NEW_PATH[@]}"; do
        [ ! "$SKIP" ] && echo -n ":"
        echo -n $NEW_ELEMENT
        SKIP=""
    done
    echo
}

######################################################
# Adds directory $1 to the pathlist $2 if it exists. #
# Also tidies up the pathlist $2                     #
# Prepends if $3 is non-zero otherwise appends.      #
######################################################
# Note: add_path seems to not work on AIX - always adds the element!
add_path() {
    local CANDIDATE AGGREGATE_NAME PATHERR PREPEND AGGREGATE
    CANDIDATE="$1"
    AGGREGATE_NAME="$2"
    PREPEND="$3"

    [ "$BH_VERBOSE" ] && echo "add_path $1 $2 $3" >&2
    if [ -z "$CANDIDATE" ] ; then
        echo "add_path: directory not specified" >&2
        return 0
    fi
    if [ ! -d "$CANDIDATE" ] ; then
        [ "$BH_VERBOSE" ] && echo "add_path: directory '$CANDIDATE' does not exist" >&2
        return 0
    fi
    if [ -z "$AGGREGATE_NAME" ]; then
        echo "add_path: aggregate not specified" >&2
        return 0
    fi

    # Note: Indirect expansion: ${!A} ... is bash (& posix?) specific
    # ${!A} == $(eval echo $`echo $A`) ... is bash (& posix?) specific
    #       == `eval echo \\$"$A"`     ... more portable - SunOS, hpux,
    #                                      aix, linux, unixware, OS5
    AGGREGATE=${!AGGREGATE_NAME}
    if [ -z "$AGGREGATE" ]; then
        export $AGGREGATE_NAME="$CANDIDATE"
    else
        # Remove leading & trailing ':', spaces around any ':', duplicate ':'
        AGGREGATE=$(echo $AGGREGATE| sed -e 's/^::*//' -e 's/::*$//' -e 's/[    ]*:[    ]*/:/g' -e 's/:::*/:/g')

        if ! in_path "$CANDIDATE" "$AGGREGATE" ;then
            if [ "$PREPEND" ]; then
                export $AGGREGATE_NAME="$CANDIDATE:$AGGREGATE"
            else
                export $AGGREGATE_NAME="$AGGREGATE:$CANDIDATE"
            fi
        fi
    fi
    [ "$BH_VERBOSE" ] && echo "$AGGREGATE_NAME=${!AGGREGATE_NAME}"
}

##################################
# Prepend $1 to path variable $2 #
##################################
pre_path() {
    add_path "$1" "$2" "prepend"
}

###############################################################
# usage: is_alive timeout hostname
# don't use this - it gives too much delay even with timeouts #
###############################################################
is_alive() {
    local PING RETVAL
    PING=""
    RETVAL=1
    case $OSTYPE in
        [lL]inux*)
            PING="ping -c 1 -w 1 $1" # -W option is not available in iputils-ss020124 (dux - RH-8)
            ;;

        hpux*)
            ;;

        solaris*)
            #       PING="ping $1 1" ... hangs indefinitely on non-existent host
            ;;

        AIX*)
            ;;

        SCO_SV)
            ;;

        UnixWare)
            ;;

        FreeBSD)
            ;;
    esac

    if [ -n "$PING" ]; then
        silent $PING
        RETVAL=$?
    fi
    return $RETVAL
}

############
# Defaults #
############
set_defaults() {
    local FN

    IGNOREEOF=0
    umask 2
    LS_OPT=""

    # RHEL-6 doesn't set this:
    [[ "$OSTYPE" ]] || export OSTYPE=$(uname -s)

    # /usr/xpg4/bin/id is for solaris
    for FN in /usr/xpg4/bin/id /usr/bin/id; do
        [ -x $FN ] && break
    done

    case `$FN -un` in
        build)
            HOME=/home/build/bhepple
            # maybe we should do a "mkdir -p $HOME" here, just in case???
            ;;
        root)
            HOME=/root
            ;;
    esac

    [ -z "$LESSOPEN" ] && [ -x $HOME/bin/lesspipe.sh ] &&  export LESSOPEN="|$HOME/bin/lesspipe.sh %s"

    GREP_OPT=""

    IGNOREEOF=0

    PATH=$(remove_dups $PATH)
    MANPATH=$(remove_dups $MANPATH)
    LD_LIBRARY_PATH=$(remove_dups $LD_LIBRARY_PATH)

    add_path "/usr/man" MANPATH
    add_path "/usr/local/man" MANPATH
    add_path "/usr/local/share/man" MANPATH
    add_path /usr/share/man MANPATH
    add_path /usr/X11R6/man MANPATH
    add_path "/usr/local/lib" LD_LIBRARY_PATH
    export LD_LIBRARY_PATH
    add_path /usr/local/bin PATH

    if silent type less; then
        export PAGER=less
        export LESS="-RiMQ"
    fi
    [ -z "$PAGER" ] && export PAGER=more

    for EDITOR in e emacsclient vim vi nano pico; do
        silent type $EDITOR && break
    done
    for XEDITOR in e emacsclient gvim gedit kate vi nano pico; do
        silent type $XEDITOR && break
    done

    # use my script to find a browser:
    BROWSER=${BROWSER:-browser}

    ID_PROG=id

    # for .emacs (value is set for us in /etc/profile.d?):
    [[ "$HOSTNAME" ]] || HOSTNAME=$( hostname )
    export HOSTNAME
    [[ "$USER" ]] || USER=$USERNAME
    export USER

    JDK=/usr/java/default
    JAVA_HOME=$JDK
    add_path $JAVA_HOME/bin PATH
    _JAVA_OPTIONS="-Dswing.aatext=true "
    _JAVA_OPTIONS+="-Dawt.useSystemAAFontSettings=lcd "
    _JAVA_OPTIONS+="-Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel "
    export _JAVA_OPTIONS

    LANG=C

    for DIR in $HOME/bin /home/bhepple/bin; do
        if [ -d $DIR ]; then
            add_path $DIR PATH prepend
            break
        fi
    done

    export EDITOR XEDITOR PATH JDK JAVA_HOME LANG MANPATH BROWSER
    export TERM_BACKGROUND="${TERM_BACKGROUND:-dark}"
    export LC_PAPER="${LC_PAPER:-A4}"
    export HISTTIMEFORMAT="%d/%m/%y %T %z "
}

#############################
# Stuff specific to this OS #
#############################
os_specific() {
    case $OSTYPE in
    [lL]inux*)
        add_path /usr/X11R6/bin PATH
        for JDK in "/opt/jdk1.2.2" "/opt/blackdown-jdk-1.4.1" ""; do
            [ -d "$JDK" ] && break
        done
        JDK_ARCH=i386
        #export GV_OPTIONS="-noantialias" # too slow
        export GREP_OPT="--directories=skip --colour=auto"
        export LS_OPT="--color=auto"
        export GDK_USE_XFT=1 # for anti-aliased fonts outside gnome
        export QT_XFT=true
        export LC_ALL=en_AU.utf8
        ;;

    hpux*)
        # JDK=/opt/java1.3 # need 1.4 for 64-bit work and 1.3 for 32 - leave
        # it to the Makefiles to decide
        # JDK_ARCH=PA_RISC
        unset JDK JDK_ARCH
        add_path /sbin PATH
        add_path /usr/sbin PATH
        export CVS_RSH=remsh
        export PTHREAD_COMPAT_MODE=1 # for B.11.22 to B.11.23 - afterwards
        # and before it appears to be 1x1 threads by default
        add_path /opt/java1.3/jre/lib/PA_RISC LD_LIBRARY_PATH
        add_path /opt/java1.3/jre/lib/PA_RISC/classic LD_LIBRARY_PATH
        add_path /opt/java1.3/jre/lib/PA_RISC/native_threads LD_LIBRARY_PATH
        stty kill ^U susp ^Z # default is kill=@ for chrissakes
        export CVS_RSH=ssh
        export RSYNC_RSH=ssh
        ;;

    solaris*)
        export MAKE=gmake
        pre_path /opt/SUNWspro/bin PATH
        add_path /usr/sbin PATH
        add_path /sbin PATH
        add_path /usr/ucb PATH
        add_path /usr/ccs/bin PATH
        add_path /etc PATH
        add_path /usr/openwin/bin PATH
        add_path /opt/netscape PATH
        add_path /bin PATH
        JDK=/usr/java1.2
        JDK_ARCH=`uname -p`
        ID_PROG="/usr/xpg4/bin/id"
        stty erase ^H # was ^?
        ;;

    AIX*)
        add_path /usr/vac/bin PATH
        add_path /usr/vacpp/bin PATH
        add_path /usr/java_dev2/jre/bin PATH
        JDK=/usr/java_dev2
        ;;

    SCO_SV)
        add_path /udk/usr/ccs/bin PATH
        add_path /etc PATH
        pre_path /usr/lib/scohelp/man MANPATH
        JDK=/usr/java2
        JDK_ARCH=x86at
        export CVS_RSH=rcmd
        stty susp ^Z # was undefined
        stty intr ^C # was DEL!
        ;;

    UnixWare)
        JDK=/usr/java2
        JDK_ARCH=x86at
        ;;

    FreeBSD)
        CPROV_ARCH=freebsd-i386
        JDK_ARCH="???"
        export LS_OPT="-G"
        ;;

    cygwin*)
        # Order is very important - we want cygwin 'find' but M$ link:
        #PATH="/cygdrive/c/WINDOWS/Microsoft.NET/Framework/v1.1.4322"
        #PATH="$PATH:/cygdrive/c/Program Files/Microsoft Visual Studio/VB98"
        #PATH="$PATH:/cygdrive/c/Program Files/Microsoft Visual Studio/Common/Tools/WinNT"
        #PATH="$PATH:/cygdrive/c/Program Files/Microsoft Visual Studio/Common/MSDev98/Bin"
        #PATH="$PATH:/cygdrive/c/Program Files/Microsoft Visual Studio/Common/Tools"
        #PATH="$PATH:/cygdrive/c/Program Files/Microsoft Visual Studio/VC98/bin"
        #PATH="$PATH:/usr/lib/lapack"
        #PATH="$PATH:/usr/local/bin:/usr/bin:/bin:/usr/X11R6/bin"
        #PATH="$PATH:/cygdrive/c/WINDOWS"
        #PATH="$PATH:/cygdrive/c/WINDOWS/system32"
        #PATH="$PATH:/cygdrive/c/WINDOWS/System32/Wbem"
        :
        ;;

    darwin*) # Mac
        add_path /opt/local/share/man MANPATH PRE
        add_path /opt/local/bin PATH PRE
        add_path /opt/local/sbin PATH PRE
        ;;

    esac

    ID=`$ID_PROG -u`
}

home_common() {
    export CVS_RSH=ssh
    export RSYNC_RSH=ssh
    export GDK_USE_XFT=1
    
    export PR=/net/nina/export/home/bhepple/tmp/probus-latest
    export PW=/mnt/probus/g-suite

    Y=2023
    for L in "" 2; do
        export P${L}WGA="$PW/1-secretary-agendas-minutes/General Meetings/Agendas - General Meetings/$Y/FINALS"
        export P${L}WGM="$PW/1-secretary-agendas-minutes/General Meetings/Minutes - General Mtg/$Y/Finalised Minutes - General Meeting Minutes"
        export P${L}WCA="$PW/1-secretary-agendas-minutes/Committee/Agendas - Committee /$Y/Finalised Committee Agendas"
        export P${L}WCM="$PW/1-secretary-agendas-minutes/Committee/Minutes - Committee/$Y/Finalised Committee Meeting Minutes"
        export P${L}WT="$PW/3-treasurer/Finance/$Y/1. Income & Expenditure"
        export P${L}WN="$PW/4-newsletter/Published/$Y"
        export P${L}WL="$PW/5-membership/Lists/$Y"
        
        export P${L}RGA="$PR/1-secretary-agendas-minutes/General Meetings/Agendas - General Meetings/$Y/FINALS"
        export P${L}RGM="$PR/1-secretary-agendas-minutes/General Meetings/Minutes - General Mtg/$Y/Finalised Minutes - General Meeting Minutes"
        export P${L}RCA="$PR/1-secretary-agendas-minutes/Committee/Agendas - Committee /$Y/Finalised Committee Agendas"
        export P${L}RCM="$PR/1-secretary-agendas-minutes/Committee/Minutes - Committee/$Y/Finalised Committee Meeting Minutes"
        export P${L}RT="$PR/3-treasurer/Finance/$Y/1. Income & Expenditure"
        export P${L}RN="$PR/4-newsletter/Published/$Y"
        export P${L}RL="$PR/5-membership/Lists/$Y"
        Y=2022
    done
    unset Y L

    export G="$HOME/fun/sf/gjots2/trunk/gjots"
    export D=~/dotfiles
    export sway=$D/.config/sway
    export kitty=$D/.config/kitty
    export emacs=$D/.config/emacs
    export emacsd=$D/.emacs.d.minimal
    export waybar=$D/.config/waybar
    export probus_lists="$probus/5-membership/Lists/2021/"
    export probus_tr="$probus/3-treasurer/Finance/2021/1. Income & Expenditure/"
}

work_common() {
    :
}

###############################
# Stuff specific to this host #
###############################
host_specific() {
    case $HOSTNAME in
    kogan)
        home_common
        ;;

    raita*) # linux or freebsd
        home_common
        # ebm only - damages emerge! export ARCH=i686
        EBM_SYNCDEV=/dev/ttyUSB0
        export EBM_SYNCDEV
        BROWSER=google-chrome
        ;;

    achar*) # linux or freebsd
        home_common
        export R=/net/nina/export/fedora-home/bhepple/podget/rn
        ;;

    nina*)
        home_common
        export R=$HOME/tmp/podget/rn
        ;;

    *) # assume we're at work
        work_common
        ;;
    esac
}

#############################################################
# OS-specific stuff that needs to run after the other stuff #
#############################################################
os_specific2() {
    case $OSTYPE in
    AIX*)
        add_path $JDK/sh PATH
        # order is crucial - put anything before $JDK/jre/bin and it core
        # dumps, it seems:
        pre_path "$JDK/jre/bin/classic" LD_LIBRARY_PATH
        pre_path "$JDK/jre/bin" LD_LIBRARY_PATH
        ;;

    *)
        if [ -n "$JDK" ] ; then
            add_path $JDK/bin PATH
            add_path $JDK/jre/lib/$JDK_ARCH LD_LIBRARY_PATH
            add_path $JDK/jre/lib/$JDK_ARCH/classic LD_LIBRARY_PATH
            add_path $JDK/jre/lib/$JDK_ARCH/native_threads LD_LIBRARY_PATH
        fi
        ;;
    esac

    case $OSTYPE in
    hpux*)
        SHLIB_PATH="$LD_LIBRARY_PATH"
        export SHLIB_PATH
        ;;
    AIX*)
        LIBPATH="$LD_LIBRARY_PATH"
        export LIBPATH
        ;;
    esac
}

########
# Main #
########

set_defaults
os_specific
host_specific
os_specific2

unset os_specific2 host_specific os_specific set_defaults pre_path
unset is_alive in_path add_path

# pull in .bashrc for login as well as non-login startup (but avoid infinite
# loop in case we're being sourced by .bashrc!):
if [ -z "$BH_BASH_RC" ]; then
    [ "$BH_VERBOSE" ] && echo ".bash_profile: sourcing .bashrc"
    source $HOME/.bashrc
    export BH_BASH_PROFILE_IMPORTS_BASHRC=yes
fi

# Ruby version manager:
[[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm" # Load RVM function

# start GUI on 'nina'
if [ "$USER" = nina ] && [ $(hostname) = nina ]; then
    if [ -z "$DISPLAY$SWAYSOCK" ]; then
        if [[ $( tty ) == "/dev/tty1" ]]; then
            exec sway-start &
        fi
    fi
fi

# anything after this line was probably added by some automation

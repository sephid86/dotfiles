# For Non-login interactive bash sessions (only)

# BH_VERBOSE=yes
[ "$BH_VERBOSE" ] && echo "$HOSTNAME:.bashrc:  TERM=$TERM"
# [ -r /etc/bashrc ] && source /etc/bashrc

# don't suppress a re-run of this as we need to over-ride things from
# /etc/bash.bashrc
# [ "$BH_BASH_RC" ] && echo ".bashrc: already done" && return

# Spurious check: If not running interactively, don't do anything
[ -z "$PS1" ] && return

##############################
# execute a command silently #
##############################
silent() {
    "$@" >/dev/null 2>&1
}

# decide if bg should be dark or light
decide_bg() {
    export TERM_BACKGROUND="${TERM_BACKGROUND:-dark}"
    [ "$TERM" ] || return
    case "$TERM" in
        rxvt*)
            for TERM in rxvt rxvt-unicode xtermc xterm; do
                silent tput colors && break
            done
            ;;
        xterm*)
            for TERM in $TERM xtermc xterm; do
                silent tput colors && break
            done
            ;;
        *)
            for TERM in $TERM vt100; do
                silent tput colors && break
            done
            TERM_BACKGROUND="dark"
            ;;
    esac
}

cheat() {
    curl "cheat.sh/$@"
}

####################
# Setup PS1 prompt #
####################

precmd() {
    local S=$?
    echo $S
    return $S
}

git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}

git_repo() {
    local FLAG; FLAG=""
    git remote -v 2>/dev/null | awk '{gsub(".*/","",$2);gsub(".git","",$2);print $2; exit}'
}

git_info() {
    R="$(git_repo)" && {
        R+=":$(git_branch)"
        [[ "$R" == ":" ]] || echo "$R"
    }
}

svn_info() {
    I=$( svn info 2>/dev/null )
    [[ "$I" ]] && {
        U=$( echo "$I" | awk '/^URL:/ {print $2}' )
        R=$( echo "$I" | awk '/^Repository Root:/ {print $3}' )
        L="${U#$R/}"
        echo "$( echo $L | awk -F/ '{print $1 "/" $2}' )"
    }
}

tbh() {
    T=$( git_info )
    [[ "$T" ]] || T="$( svn_info )"
    [[ "$T" ]] && T="( $T ) "
    echo "$T"
}

#
# Note that you can tell readline about characters in PS1 that take no space
# (eg escape sequences) by bracketing them with \[ ... \]
#
# \e = escape
# \A = time HH:MM
# \W = basename of PWD
#
# this affect the window title as well as what appears on the command line:
setup_prompt() {
    local H TITLEBAR BLACK RED GREEN YELLOW BLUE MAGENTA CYAN WHITE BOLD NORM NL
    local OS U

    [[ "$TERM" ]] || return
    silent tput colors || return

    H=""   # host
    U=""   # user
    OS=""  # os
    NL="\\n"  # newline

    [[ "$SSH_TTY$SSH_CLIENT$VNCDESKTOP" ]] && H="\\h:"

    case "$OSTYPE" in
        [Ll]inux*)
            OS=""
        ;;
        hpux*) # can't seem to turn off horizontal-scroll-mode so use NL
            # maybe the \[ ... \] has fixed this?
            NL='\n'
            OS="$OSTYPE:"
        ;;
        *)
            OS="$OSTYPE:"
        ;;
    esac

    case $USER in
        bhepple)
            ;;
        *)
            U="\\u@"
            ;;
    esac

    case $TERM in
        *rxvt* | xterm* | alacritty | kitty | foot)
            # ESC]2;stringBEL -- Set window title to string
            # none of this prints in PS1
            TITLEBAR=$'\e'    # escape
            TITLEBAR+="]2;$U$H$OS\\W"
            TITLEBAR+=$'\007' # BELL
            ;;
        *)
            TITLEBAR=''
            ;;
    esac

# konsole can take the tabname from the term title anyway, so no need for this:
#    if env | silent grep ^KONSOLE; then
#        TABNAME="\\[]30;\\]${HOSTNAME}\\[\\]"
#    fi

    if [ "$( tput colors )" -lt 8 ]; then
        export PS1="$U$H\\W/:$NL\\$ "
    else
#       Color       #define       Value       RGB
#       black     COLOR_BLACK       0     0, 0, 0
#       red       COLOR_RED         1     max,0,0
#       green     COLOR_GREEN       2     0,max,0
#       yellow    COLOR_YELLOW      3     max,max,0
#       blue      COLOR_BLUE        4     0,0,max
#       magenta   COLOR_MAGENTA     5     max,0,max
#       cyan      COLOR_CYAN        6     0,max,max
#       white     COLOR_WHITE       7     max,max,max

        # setaf = set foreground
        # setab = set background
        # red, green, cyan and magenta seems to be good in both black and white backgrounds
        BLACK=$(   tput setaf 0 )
        RED=$(     tput setaf 1 )
        GREEN=$(   tput setaf 2 )
        YELLOW=$(  tput setaf 3 )
        BLUE=$(    tput setaf 4 )
        MAGENTA=$( tput setaf 5 )
        CYAN=$(    tput setaf 6 )
        WHITE=$(   tput setaf 7 )
        BOLD=$(    tput bold    )
        NORM=$(    tput sgr0    ) # turn off all attributes
        #if [ "$TERM_BACKGROUND" = "dark" ]; then
        #    PS1="\\[$BOLD$YELLOW\\]\\A $U\\[$MAGENTA\\]$H\$( tbh )\\[$CYAN\\]\\W/ \\[$NORM\\]$NL\\$ "
        #else
        #    PS1="\\[$BOLD$BLACK\\]\\A $U\\[$RED\\]$H\$( tbh )\[$BLUE\\]\\W/ \\[$NORM\\]$NL\\$ "
        #fi
        PS1="\\[$BOLD$CYAN\\]\\A $U\\[$RED\\]$H\$( tbh )\[$GREEN\\]\\W/ \\[$NORM\\]$NL\\$ "
    fi

    # put it all together:
    PS1="\$( precmd ) \\[$TITLEBAR\\]$PS1"
}

do_unaliases() {
    local COM
    for COM in rm cp mv lsf ll l ls h v s; do
        silent unalias $COM
    done
}

hist() { history |grep -- ${@:-.}; }

mkcd() { [[ "$1" ]] && mkdir -p "$1" && cd "$1"; }

do_aliases() {
    alias s=silent
    alias ls="ls $LS_OPT"
    alias lsf="ls -CF $LS_OPT"
    alias l="ls -CF $LS_OPT"
    alias ll="ls -lF $LS_OPT"
    alias l.='ls -d .* --color=auto'

    alias mcd=mkcd
    #alias rm='rm -i'
    alias cp='cp -i'
    alias mv='mv -i'
    #stty erase ^h
    alias grep="grep $GREP_OPT"
    alias egrep="egrep $GREP_OPT"
    alias fgrep="fgrep $GREP_OPT"
    alias xxgdb='LD_LIBRARY_PATH=/usr/X11R6/lib/libXaw xxgdb'
    # needed for bind9: alias nslookup='nslookup -sil'
    alias mpage='mpage -P-' # make it filter!
    alias enscript='enscript -p -' # make it filter!
    alias gv="gv $GV_OPTIONS"
    if silent type v; then
        :
    else
        alias v=$PAGER
    fi
    #alias nedit="nedit -geometry 88x74+0+0"
    if silent type vim; then
        if  [ "$TERM_BACKGROUND" ]; then
            alias vi="vim \"+set bg=\$TERM_BACKGROUND\""
        else
            alias vi="vim"
        fi
    fi
    alias lsmp3='gvmp3-info -f "%f %nTALB - %nTIT2\n"'
}

do_host_aliases() {
    case $HOSTNAME in
    bhepple-pc*|dancer*)
        alias root="sudo"
    ;;
    esac
}

do_os_aliases() {
    case $OSTYPE in
    hpux*)
        alias rsh="remsh"
    ;;
    SCO_SV)
        alias rsh="rcmd"
    ;;
    esac
}

final_custom() {
    local FN
    # ~bhepple doesn't work on hpux at login for some reason
    FN=$HOME/bin/acd-func.sh
    [ -r $FN ] && source $FN

    FN=$HOME/.dir_colors
    [ -r $FN ] && silent type dircolors && eval $( dircolors $FN )
}

########
# Main #
########

export BH_BASH_RC="yes"

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

if [ -z "$BH_BASH_PROFILE" ]; then
    [ "$BH_VERBOSE" ] && echo ".bashrc: sourcing .bash_profile:"
    export BH_BASHRC_IMPORTS_BASH_PROFILE=yes
    source .bash_profile
fi

# mainly for kitty which has TERM=xterm-kitty which is mostly unknown!
silent tput colors || TERM=xterm

[ "$TERM" -a "$TERM" != dumb ] && {
    [ "$TERM_BACKGROUND" ] || decide_bg
    setup_prompt
}
do_unaliases
do_aliases
do_host_aliases
do_os_aliases
final_custom

unset do_unaliases do_aliases do_host_aliases do_os_aliases final_custom
unset decide_bg

#GPG_TTY=$(tty)
#export GPG_TTY
shopt -s histappend
shopt -s direxpand # revert to pre-4.2 behaviour otherwise 'cd $P/<tab>' expands to 'cd \$P/'

# PROMPT_COMMAND is set in /etc/bashrc:
PROMPT_COMMAND="history -a;history -c; history -r;$PROMPT_COMMAND"
# don't remember lines starting with a space or duplicates:
HISTCONTROL="ignoreboth:erasedups"

[[ "$SWAYSOCK" ]] && dark-mode --term-only refresh

# source ~/bin/bash_completion
# _bcpp --defaults

################################################################################

# fzf tweaks:
type fzf &>/dev/null && {
    export FZF_DEFAULT_OPTS="--no-mouse "
    source /usr/share/fzf/shell/key-bindings.bash
    # restore C-t binding trashed by fzf:
    bind -m emacs-standard '"\C-t": transpose-chars'
    # CTRL-V - Paste the selected file path into the command line (C-v was quoted-insert)
    bind -m emacs-standard '"\C-v": " \C-b\C-k \C-u`__fzf_select__`\e\C-e\er\C-a\C-y\C-h\C-e\e \C-y\ey\C-x\C-x\C-f"'
    # restore M-c binding trashed by fzf:
    bind -m emacs-standard '"\ec": capitalize-word'
    # ALT-V - cd into the selected directory (M-v was unassigned)
    bind -m emacs-standard '"\ev": " \C-b\C-k \C-u`__fzf_cd__`\e\C-e\er\C-m\C-y\C-h\e \C-y\ey\C-x\C-x\C-d"'

    type ccd &>/dev/null && source ccd
    export CCD_POST='pwd;ls -lrt'

    # if C-r is aborted with Esc, don't lose what we've already typed:
    bind -m emacs-standard -x '"\C-r":  FZF_DEFAULT_OPTS+="--print-query --bind esc:print-query+abort" __fzf_history__'
    
    # add -xdev to limit searches:
    export FZF_ALT_C_COMMAND="command find -L . -xdev -mindepth 1 \\( -path '*/\\.*' -o -fstype 'sysfs' -o -fstype 'devfs' -o -fstype 'devtmpfs' -o -fstype 'proc' \\) -prune -o -type d -print 2> /dev/null | cut -b3-"

    # see https://github.com/lincheney/fzf-tab-completion
    # source ~/bin/fzf-bash-completion.sh
    # bind -x '"\t": fzf_bash_completion'
}

[ "$BH_VERBOSE" ] && echo "$HOSTNAME:.bashrc: finished"

# anything after this line was probably added by some automation

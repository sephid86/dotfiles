This is a bunch of emacs stuff that doesn't come pre-packaged.

Some of it may be very home-made.

See also my main configuration in ~/dotfiles/.emacs.d.minimal/config.el

I make a soft link in $HOME:

ln -s .emacs.d.minimal .emacs.d

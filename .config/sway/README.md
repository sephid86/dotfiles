# sway - my config

Here be notes on my **sway**(1) config.

Note that many functions use the scripts in the ../../bin directory so
make sure they are on your $PATH before trying this.

![screen image](screen.png)

As you can see, there's little to talk about on my screen dump -
there's the waybar, of course, but that's pretty much the only rice.

Other than that, there's emacs and foot - tiled so the background is
rarely visible. When it is, it looks like this, care of
[wlr-sunclock](https://github.com/sentriz/wlr-sunclock):

![background](background.png)

## Startup

I generally start from a console rather than a Display Manager, using
my script **sway-start**(1). Once **sway**(1) is going, it runs my script
**sway-start-apps**(1) which is a bash script coded to start up my normal
session. On the laptop, this is **emacs**(1) and 2 **foot**(1) windows on W/S 1,
**firefox**(1) on W/S 2, **mythtvfrontend**(1) on W/S 3 and multi-media widgets (**pavucontrol**(1)
and **blueman-manager**(1)) on W/S 4.

It can be tricky to get these started in a script without interfering
with each other, so I wrote a python script
[sway-toolwait](https://gitlab.com/wef/dotfiles/-/blob/master/bin/sway-toolwait)
to serialise the various operations - in memory of the ancient
**xtoolwait**(1) of SunOS days.

## Menu

It's hard to remember _all_ the key bindings, so the bindsym
configuration file is annotated with comments such as:

```
#### _Window Move window down
    bindsym  $mod+$s+Down  move down
```

This is picked up by the python script
[sway-menu](https://gitlab.com/wef/dotfiles/-/blob/master/bin/sway-menu)
which is bound to `$mod+Shift+Menu` and which displays a menu of
commands such as:

![sway-menu](sway-menu.png)

Easy-peasy

## Drop-down terminal

For casual use, it's nice to use a drop-down terminal. For this I run:

```
foot --app-id=dropdown-term --window-size-chars 90x10
```

with a rule:

```
for_window [app_id="dropdown-term"]             floating enable, $slim_border
```

I also run it via my script **sway-focus**(1) to position it at the origin (I don't
want to use a rule for that) but also to pull/push it to the
scratchpad if it's already running. This gives a fast starting terminal.

## Configuration

This is broken into include files each of which has a `exec_always` to
do a little bit of logging.

I tend to use **wofi**(1) rather than **dmenu**(1).

## Key bindings - bindsym

Fairly close to the 'canonical' /etc/sway/config. Although ...

I prefer that all unmodified and **Alt+**, **Control+**, **Shift+Alt+**,
**Shift+Control+** key bindings belong to the application and are not
consumed by **sway**(1). The one exception is the well-known
**Alt+Tab** sequence which is serviced by a bespoke python script
[sway-select-window](https://gitlab.com/wef/dotfiles/-/blob/master/bin/sway-select-window).

**Mod4** (the logo key) is the primary modifier for WM operations.

All WM operations use **Mod4+** or **Mod1+Control+** (ie **Alt+Control+**)

The reason for the duplication is in case I need to use a keyboard without a logo key.

For example:

To move the focus to the window at the left:

```
bindsym $mod+Left  focus left
```

is duplicated as:

```
bindsym Mod1+Control+Left  focus left
```

thus:

```
$mod+       == Mod1+Control+
$mod+Shift+ == Mod1+Control+Shift+ often the opposite or stronger than $mod+
```

the following have no **Mod1+Control+** equivalents, so I stay away from them as much as possible:

```
$mod+Control+
$mod+Control+Shift+
$mod+Mod1+
$mod+Mod1+Shift+
$mod+Mod1+Control+
$mod+Mod1+Control+Shift
```

... they're too hard to remember anyway, except through **sway-menu**(1)
... exceptions are marked with `# NB: no Mod1+Control+ version`

### headless

To run a remote headless **sway**(1) session:

```
sway --config ~/.config/sway/headless
```

To connect to it:

```
vncviewer <SERVER_NAME>::5900
```

Sadly (because it uses **Xwayland**(1)) **vncviewer**(1) is still the
most performant VNC viewer.

### exec

### misc

I like to have a bright 'indicator' (the thin line at the edge of a
window showing where it would split) as I can't see the default colours.

### modes

#### Resize

$mod+r has the usual `left`/`right`/`up`/`down` functions plus s(mall),
m(edium) and l(arge) and 2, 3, 4, 6, 8 and 9 which resize a floater to
1/2, 1/3, 1/4 ./etc of the screen.

#### Swap

$mod+s is swap mode - not in the default config. `left`/`right`/`up`/`down`
swaps the focus window with the one in that direction.

#### Move

$mod+m moves windows around - `left`/`right`/`up`/`down` as you would expect.
h,j,k and l are like the **vi**(1) motion keys.

These keys reflect the position on the keyboard:

```
    w e r
    s d f
    x c v
```

ie top-left, top-centre, top-right etc

&gt; and &lt; take the window to the next/prev empty workspace as do n and p

#### Move to Output

I rarely use an external monitor, but this mode allows operations to different outputs.

`left`, `right`, `up` and `down` changes the focus to another output. The
shifted version takes the focused window with you.

`PgUp`, `PgDn`, `Home` and `End` move the entire workspace to the appropriate output.

#### Passthrough

This is mainly for use with remote VNC sessions - when a remote
session starts, I usually make it fullscreen and then press $mod+Mute
so that all keystrokes apply to the remote.

## waybar

See [waybar](https://gitlab.com/wef/dotfiles/-/tree/master/.config/waybar)
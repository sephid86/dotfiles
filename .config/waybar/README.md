# waybar

![bar image](bar.png)*bar image (larger font than usual, for display purposes)*

The bar should normally be unobstrusive, maybe even hidden with
SIGUSR1. Grey is dull enough to not be noticed. Similarly, the blue.
Alarms come up in a blinking fiery orange. At all events, I avoid the
angry fruit salad.

Significantly more information is available in the tooltip - if I want
it, I'll hover over the button.

Clickable buttons are documented in the tooltip.

The ordering and min-width of the buttons is designed to reduce the
'dancing' effect when each button is updated. I don't want it to
distract me unless something's wrong.

The workspace names are updated by the script ../../bin/sway-track-prev-focus

The reload button gives an alarm when one of the waybar scripts or
configs has changed - a reminder to send waybar a SIGUSR2.

The hamburger button is presently linked to xfce4-appfinder.

## Config
- config: waybar configuration
- style.css: stylesheet

## scripts

- bandwidth: upload and download stats. Click -> L:nethogs.
- btc: crypto monitor. Click -> L:xdg-open https://binance.com
- hostname: shows the current hostname. Click -> L:wofi menu for remote desktops.
- packages: how many packages need an update (fedora). Click -> dnfdragora
- vpn: vpn state and country code eg AU=Australia). Click -> L:toggles it on and off. Alarms if vpn is off and torrenting is active. tooltip shows hostname, Country, City, technology.
- weather: local weather. Click -> gnome-weather. toolip shows mina, current, max temperature for the day, wind direction & speed, sunrise & sunset.

## other buttons

- pulseaudio. Click -> L:pavucontrol, M:equaliser, R:blueman
- cpu. Click -> L:gnome-system-monitor
- RAM. Click -> L:gnome-system-monitor
- Temp. Click -> L:sensors
- Battery. Click -> M:toggle-devices R:gnome-control-center power
- Disk. Click -> L:gnome-disks
- Clock. Click -> R:gnome-disks
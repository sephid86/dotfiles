;;;* Navigation in this file:
;; Use TAB / S-TAB to cycle visibility (outline-minor-mode) on header lines
;; Use <f1> to cycle visibility in body

;;;* TODO:
;;;** C-h m still invokes describe-mode which leaves the cursor in the original buffer - needs C-x 1
;;;* Prelims

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Bob Hepple"
      user-mail-address "bob.hepple@gmail.com")

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Sync/")
(setq org-replace-disputed-keys t)
(setq org-disputed-keys '(([(shift up)]            .  [(meta p)])
                          ([(shift down)]          .  [(meta n)])
                          ([(shift left)]          .  [(meta -)])
                          ([(shift right)]         .  [(meta +)])
                          ([(control shift right)] .  [(meta shift +)])
                          ([(control shift left)]  .  [(meta shift -)])
                          ([(control shift up)]    .  [(meta shift \))])))
;                          ([(control shift down)]  .  [(meta shift \()])
;                          ([(kbd "C-'")]           .  [(kbd "C-\"")])))

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)

;; `(online?)` is a function that tries to detect whether you are online.
;; We want to refresh our package list on Emacs start if we are. (from ohai)
(require 'cl)
(defun online? ()
  (if (and (functionp 'network-interface-list)
           (network-interface-list))
      (cl-some (lambda (iface) (unless (equal "lo" (car iface))
                         (member 'up (first (last (network-interface-info
                                                   (car iface)))))))
            (network-interface-list))
    t))

;;;* local elisp
;; for elisp not in MELPA:
(setq load-path (append load-path '("~/.config/emacs")))
;;;* Packages
;;;** initialise package system
(setq package-user-dir (concat dotfiles-dir "elpa"))
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/") t)
(package-initialize)
(when (online?)
  (unless package-archive-contents (package-refresh-contents)))
;; (package-install 'use-package) ; built-in emacs-29
(setq use-package-always-ensure t)

;;;** load packages
;;;*** ripgrep
(use-package ripgrep
  ;; :defer t fails to load
  :config
  (grep-apply-setting 'grep-find-command
                      '("rg --follow -n -H --no-heading --hidden -e '' ." . 36)))
;; this variant searches from the top level if in a git directory:
;;                      '("rg -n -H --no-heading -e '' $(git rev-parse --show-toplevel 2>/dev/null || pwd)" . 27)))
;;;*** whitespace
(use-package whitespace
  ;; :defer t fails to load
  :config
  (whitespace-mode -1))
(setq bh/whitespace-mode "off")
;;;*** helpful
;; from doom:
(use-package helpful
  ;; a better *help* buffer
  ;; :defer t fails to load - although :commands implies :defer!!
  :commands helpful--read-symbol
  :init
  (global-set-key [remap describe-function] #'helpful-callable)
  (global-set-key [remap describe-command]  #'helpful-command)
  (global-set-key [remap describe-variable] #'helpful-variable)
  (global-set-key [remap describe-key]      #'helpful-key)
  (global-set-key [remap describe-symbol]   #'helpful-symbol))
;;;*** desktop
(use-package desktop
  ;; :defer t fails to load
  :config
  (desktop-save-mode 1))
;(desktop-read) ... not actually needed - in fact it raises an error thinking desktop is already running!

;;;*** magit
(use-package magit :defer t)

;;;*** vc
(use-package vc :defer t)
;;;*** browse-kill-ring
(use-package browse-kill-ring
  :defer t
  :config
  (browse-kill-ring-default-keybindings))

;; Get an instant cheat sheet for your current major mode
;; with C-h C-m.
(use-package discover-my-major
  :defer t
  :commands (discover-my-major discover-my-mode)
  :bind ("C-h C-m" . discover-my-major))

;;;*** which-key
(use-package which-key
  ;; :defer t fails to load
  :commands which-key-mode
  :demand t
  :config
  (which-key-mode)
  ;; Set the delay before which-key appears.
  (setq-default which-key-idle-delay 1.0)
  ;; which-key will truncate special keys by default, eg. SPC turns into
  ;; an orange D. Turn this off to avoid confusion.
  (setq-default which-key-special-keys nil)
  ;; Hit C-h C-k to have which-key show you all top level key bindings.
  :bind ("C-h C-k" . which-key-show-top-level)
  :diminish which-key-mode)

;;;*** ido

(ido-mode t)
(setq ido-enable-prefix nil
      ido-enable-flex-matching t
      ido-create-new-buffer 'always
      ido-use-filename-at-point nil
      ido-use-url-at-point nil
      ido-max-prospects 10
      ido-use-virtual-buffers t
      ido-enable-last-directory-history t)
;; others?:
;; ido-enable-regexp nil
;; ido-max-directory-size 300000
;; ido-max-file-prompt-width 0.1

;; so that ido works on insert-char (which has 42k unicode symbols)
;; see https://github.com/DarwinAwardWinner/ido-completing-read-plus/issues/56
(setq ido-cr+-max-items 45000)

;; Make sure ido is really everywhere.
(use-package ido-completing-read+
  ;; :defer t fails to load
  :config
  (ido-ubiquitous-mode 1))

;; Use smex to provide ido-like interface for M-x
(use-package smex
  :defer t
  :config
  (smex-initialize)
  :bind (("M-x" . smex)
         ("M-X" . smex-major-mode-commands)
         ;; This is the old M-x.
         ("C-c C-c M-x" . execute-extended-command)))

;; Vertical ido.
(use-package ido-vertical-mode
  ;; :defer t fails to load
  :config
  (ido-vertical-mode))
(setq ido-vertical-define-keys 'C-n-C-p-up-down-left-right)

;; Improved fuzzy matching.
(use-package flx-ido
  :defer t
  :config
  (flx-ido-mode 1)
  (setq ido-enable-flex-matching t
        ido-use-faces nil
        gc-cons-threshold 20000000))

;; fix ido-find-file:
;; https://superuser.com/questions/312723/how-to-break-out-of-ido-find-files-searching-for
(add-hook 'ido-setup-hook 'shab-ido-config)
(defun shab-ido-config()
 ;; ... other ido-config here ...
 ;; disable auto searching for files unless called explicitly with C-c C-s
  (setq ido-auto-merge-delay-time 99999)
  (define-key ido-file-dir-completion-map (kbd "C-c C-s")
    (lambda()
      (interactive)
      (ido-initiate-auto-merge (current-buffer)))))

;;;*** ibuffer-vc
(use-package ibuffer-vc :defer t) ; for gk-ibuffer.el
;; better ibuffer format:
(load-file "~/.config/emacs/gk-ibuffer.el")

;;;*** rpm-spec-mode
;; don't use the one in elpa - it is abandoned. It's forked here: https://github.com/vmfhrmfoaj/rpm-spec-mode
;; (use-package rpm-spec-mode
;;   :defer t
;;   :init
;;   (setq auto-mode-alist (append '(("\\.spec" . rpm-spec-mode))
;;                                 auto-mode-alist)))
(require 'rpm-spec-mode)
(setq auto-mode-alist (append '(("\\.spec" . rpm-spec-mode)) auto-mode-alist))

;;;*** esup
(use-package esup
  :ensure t
  ;; To use MELPA Stable use ":pin melpa-stable",
  :pin melpa
  :commands (esup))

;;;*** neotree
(use-package neotree
  :bind (:map neotree-mode-map
              ("v" . (lambda () (interactive)
                       (neotree-quick-look)
                       (neotree-hide)))))

;;;*** elfeed
(use-package elfeed
  :bind (:map elfeed-search-mode-map
              ("s" . elfeed-search-set-filter)
              ("S" . elfeed-search-live-filter))
  :config (setf elfeed-sort-order 'ascending))

(use-package elfeed-dashboard
  :ensure t
  :bind ("C-x w" . 'elfeed-dashboard)
  :config
  (setq elfeed-dashboard-file "~/.config/emacs/elfeed-dashboard.org")
  ;; update feed counts on elfeed-quit
  (advice-add 'elfeed-search-quit-window :after #'elfeed-dashboard-update-links))
(setq elfeed-feeds '(
                     ("http://xkcd.com/rss.xml" comic xkcd)
                     ("http://rss.slashdot.org/Slashdot/slashdotMain" slashdot)
                     ("https://www.reddit.com/r/emacs/new/.rss?sort=new" reddit emacs)
                     ("https://www.reddit.com/r/linux/new/.rss?sort=new" reddit linux)
                     ("https://rss.beehiiv.com/feeds/iiTciQgHPG.xml" fossweekly linux)
                     ("https://distrowatch.com/news/dw.xml" news distrowatch linux)
                     ("http://oglaf.com/feeds/rss/" comic oglaf)
                     ("https://www.reddit.com/r/linuxquestions/new/.rss?sort=new" reddit linuxquestions)
                     ("https://www.reddit.com/r/fedora/new/.rss?sort=new" reddit fedora)
                     ("https://feeds.bbci.co.uk/news/rss.xml" news bbc)
                     ("https://www.rt.com/rss/" news rt)
                     ("https://hnrss.org/frontpage?count=100" news hacker)
                     ("https://www.reddit.com/r/swaywm/new/.rss?sort=new" reddit sway)
                     ("https://www.reddit.com/r/i3wm/new/.rss?sort=new" reddit i3wm)
                     ("https://www.reddit.com/r/nordvpn/new/.rss?sort=new" reddit nordvpn)
                     ("https://lwn.net/headlines/rss" news LWN.net)
                     ("https://lobste.rs/t/linux.rss" news lobsterlinux linux)
                     ("https://www.emacswiki.org/emacs?action=rss" news EmacsWiki emacs)
                     ("https://feeds.arstechnica.com/arstechnica/index" news arstechnica)
                     ("https://www.engadget.com/rss.xml" news engadget)
                     ("file:///home/bhepple/tmp/apnews" news apnews)))
;;(use-package elfeed-goodies) ; Nah! messes up pgtk-28.0.50 keyboard input. FIXME

;; download AP RSS feeds to a local file before running elfeed-dashboard-update:
(advice-add 'elfeed-dashboard-update
            :before
            (lambda ()
              (shell-command "ratt auto https://apnews.com/hub/world-news > ~/tmp/apnews")))

;; this is used to remove 'sport' items from BBC feed:
(defun bh/elfeed-untag-by-regex( feed regex )
  "Untag items matching REGEX from FEED and view the result"
  (elfeed-search-set-filter (concat "+unread +" feed " " regex))
  (elfeed)
  (mark-whole-buffer) ;; FIXME - we're not supposed to use this - but what?
  (elfeed-search-untag-all-unread)
  (elfeed-search-set-filter (concat "+unread +" feed)))

;; from: https://github.com/skeeto/elfeed/issues/392
(defun sk/elfeed-db-remove-entry (id)
  "Removes the entry for ID"
  (avl-tree-delete elfeed-db-index id)
  (remhash id elfeed-db-entries))

(defun sk/elfeed-search-remove-selected ()
  "Remove selected entries from database"
  (interactive)
  (let* ((entries (elfeed-search-selected))
	 (count (length entries)))
    (when (y-or-n-p (format "Delete %d entires?" count))
      (cl-loop for entry in entries
	       do (sk/elfeed-db-remove-entry (elfeed-entry-id entry)))))
  (elfeed-search-update--force))

;;;*** eglot
(require 'eglot) ;; in ~/.config/emacs/eglot.el from https://github.com/joaotavora/eglot
(add-hook 'c-mode-hook #'eglot-ensure)
(add-hook 'c++-mode-hook #'eglot-ensure)
(add-hook 'python-mode-hook #'eglot-ensure)

;; (use-package jsonrpc :ensure t)
;; (use-package flymake :ensure t)
;; (use-package project :ensure t)
;; (use-package xref :ensure t)
;; (use-package eldoc :ensure t)

;; it turns out that speedbar is better:
;;;*** imenu-list
;(use-package imenu-list
;  :bind
;  ("C-'" . 'imenu-list-smart-toggle)
;  :config
;  (setq imenu-list-position 'left)
;  (add-hook 'python-mode-hook #'(lambda() (imenu-list-minor-mode 1)))
;  (add-hook 'c-mode-hook #'(lambda() (imenu-list-minor-mode 1)))
;  (add-hook 'c++-mode-hook #'(lambda() (imenu-list-minor-mode 1))))

;;;*** speedbar
(speedbar)
(setq speedbar-show-unknown-files t)
(setq speedbar-directory-unshown-regexp "^\\(\\.*\\)\\'")
;;;*** sr-speedbar
;;; NB this has a curious interaction with helpful - when you
;;; quit-window from helpful, the window is not zapped so you end up
;;; with multiple windows. speedbar is OK.
;(use-package sr-speedbar)
;  :bind
;  ("s-s" . 'sr-speedbar-toggle))
;;;*** bookmark+
;;; mainly for the ability to edit bookmarks in emacs with 'e'
;;; I just fail to grok his 'tags'
;; https://www.emacswiki.org/emacs/BookmarkPlus
;; update with:
;; cd ~/dotfiles/.config/emacs/bookmark+
;; for i in bookmark+-1.el  bookmark+-bmu.el  bookmark+.el  bookmark+-key.el  bookmark+-lit.el  bookmark+-mac.el; do wget -N https://www.emacswiki.org/emacs/download/$i; done
(setq load-path (append load-path '("~/.config/emacs/bookmark+")))
;; (autoload 'bmkp-same-file-p "bookmark+-1")
;; (autoload 'bookmark-bmenu-list "bookmark+-bmu")
(require 'bookmark+)

;;;*** browse-kill-ring+
;; nothing of real value here? Maybe get rid of it.
;; https://www.emacswiki.org/emacs/browse-kill-ring+.el
;; update with:
;; cd ~/dotfiles/.config/emacs/browse-kill-ring+
;; for i in *.el; do wget -N https://www.emacswiki.org/emacs/download/$i; done
;; NB seems to break browse-kill-ring-forward and browse-kill-ring-previous
;(setq load-path (append load-path '("~/.config/emacs/browse-kill-ring+")))
;(require 'browse-kill-ring+)
;;;*** projectile
;; I'm not using projectile ATM but just in cas I change my mind, this was useful:
;; to make C-c p p report a sorted list of projects:
;; (defun bh/projectile-sort-projects (projects)
;;   "Return the sorted list of projects that `projectile-relevant-known-projects'
;; returned."
;;   (sort projects 'string<))
;; (advice-add 'projectile-relevant-known-projects :filter-return #'bh/projectile-sort-projects)
;;;*** key-chords
(use-package key-chord
  :config (key-chord-mode 1))
;;
;; and some chords: at least check /usr/share/dict/words
;;

(key-chord-define-global ";'"     'delete-other-windows) ;; was bh/kill-next-window) ;; C-x 1
;; (key-chord-define-global ",."  'kmacro-end-and-call-macro) ;; C-x e
(key-chord-define-global "qq"     'speedbar)
;;(key-chord-define-global "qq"     #'(lambda () (interactive) (dired ".")))
(key-chord-define-global "ww"     'neotree-find)
(key-chord-define-global "zx"     'bh/buffer-menu)
(key-chord-define-global "xc"     'ibuffer)
;(key-chord-define-global "DD"     'bh/kill-whole-line) ; 'dd' is too common!
;(key-chord-define-global "yy"     'bh/yank-whole-line) ; 'YY' is too much of a stretch
;(key-chord-define-global "CC"     'bh/copy-whole-line) ; 'cc' is too common!

;; these are just too annoying:
;; (key-chord-define-global ",."     "<>\C-b")
;; (key-chord-define-global "''"     "''\C-b")
;; (key-chord-define-global "\"\""   "\"\"\C-b")
;; (key-chord-define-global "<>"     "<>\C-b")
;; (key-chord-define-global "[]"     "[]\C-b")
;; (key-chord-define-global "{}"     "{}\C-b")
;; (key-chord-define-global "()"     "()\C-b")
(key-chord-define-global "hk"     'describe-key)
(key-chord-define-global "hf"     'describe-function)
(key-chord-define-global "hv"     'describe-variable)

;;;*** org-mode
;;;**** org-capture
(setq org-capture-templates
      (quote (("c" "addr" entry (file "~/tmp/addr.org")
"* %?
:PROPERTIES:
:POSITION:
:COMPANY:
:CODE:
:ADDR1:
:ADDR2:
:ADDR3:
:ADDR4:
:TEL:
:FAX:
:XMAS:
:EMAIL:
:NOTES:
:PRINTFLAG:
:SALUTATION:
:DATESTAMP:
:MOBILE:
:END:
"))))

;;;**** fixup org-mode's trashing of case-fold-search:
(add-hook 'org-mode-hook
          (lambda () (setq case-fold-search t)))
;;;*** hideshow mode
;; from /usr/share/emacs/29.0.50/lisp/progmodes/hideshow.el.gz:
(defvar my-hs-hide nil "Current state of hideshow for toggling all.")
(defun my-toggle-hideshow-all () "Toggle hideshow all."
  (interactive)
  (setq my-hs-hide (not my-hs-hide))
  (if my-hs-hide
      (hs-hide-all)
    (hs-show-all)))

;; https://lists.gnu.org/archive/html/emacs-devel/2011-04/msg00562.html
;; called once on a line that contains a hidden block, shows the
;; block; otherwise calls the default action of TAB; called twice on a
;; line that does not contain a hidden block, hide the block from the
;; current position of the cursor
(defun tab-hs-hide ( &optional arg )
  (interactive "P")
  (let ((sl (save-excursion (move-beginning-of-line nil) (point) ) )
        (el (save-excursion (move-end-of-line nil) (point) ) )
        obj)
    (catch 'stop
      (dotimes (i (- el sl))
        (mapc
         (lambda (overlay)
           (when (eq 'hs (overlay-get overlay 'invisible))
             (setq obj t)))
         (overlays-at (+ i sl)))
        (and obj (throw 'stop 'stop) ) ) )
    (cond ((and (null obj)
                (eq last-command this-command) )
           (hs-hide-block) )
          (obj
           (progn
             (move-beginning-of-line nil)
             (hs-show-block) ) )
          (t
           (save-excursion
             (funcall (lookup-key (current-global-map) (kbd "^I") ) arg ) ) ) ) 
    ) )

(add-hook 'hs-minor-mode-hook
          (lambda ()
            (define-key hs-minor-mode-map [tab] 'tab-hs-hide )
            (define-key hs-minor-mode-map [backtab] 'my-toggle-hideshow-all)))

;;;*** outline-minor-mode
;; This makes outline-minor-mode operate a bit more like org-mode <tab> & S-<tab>

;; this enables C-c C-c to be used instead of the very awkward C-c C-@ prefix
(with-eval-after-load "outline"
  (define-key outline-minor-mode-map (kbd "C-c C-c")
              (lookup-key outline-minor-mode-map (kbd "C-c @"))))

(add-hook 'outline-minor-mode-hook
          (lambda ()
            (define-key outline-minor-mode-map (kbd "C-c C-n") 'outline-next-visible-heading)
            (define-key outline-minor-mode-map (kbd "C-c C-p") 'outline-previous-visible-heading)
            (define-key outline-minor-mode-map (kbd "C-c C-f") 'outline-forward-same-level)
            (define-key outline-minor-mode-map (kbd "C-c C-b") 'outline-backward-same-level)
            (define-key outline-minor-mode-map (kbd "C-c C-u") 'outline-up-heading)
            (define-key outline-minor-mode-map (kbd "C-c C-a") 'outline-show-all)
            (define-key outline-minor-mode-map (kbd "C-c C-c C-a") 'outline-show-all)
            (define-key outline-minor-mode-map (kbd "<f1>") 'outline-toggle-children)
            (setq outline-minor-mode-cycle t)))

;;;*** company-mode
(use-package company
  :ensure t
;;  :after lsp-mode
;;  :hook (lsp-mode . company-mode)
  :custom
  (company-minimum-prefix-length 3)
  (company-idle-delay 1.0))
(add-hook 'after-init-hook 'global-company-mode)
;;;*** lsp-mode (with pylsp (fedora pkg) or pyright (npm package))
;; https://emacs-lsp.github.io/lsp-mode/page/performance/
;; (setq read-process-output-max (* 1024 1024)) ;; 1mb
;; (setq gc-cons-threshold 100000000)
;;
;; (use-package lsp-mode
;;   :ensure t
;;   :commands (lsp lsp-deferred)
;;   :init
;;   (setq lsp-keymap-prefix "C-c l")
;;   :custom
;;   (lsp-enable-which-key-integration t)
;;   (lsp-headerline-breadcrumb-enable t)
;;   (lsp-enable-indentation nil)
;;   (lsp-enable-on-type-formatting nil)
;;   (lsp-modeline-code-actions-enable nil)
;;   (lsp-modeline-diagnostics-enable t)
;;   (lsp-clients-clangd-args '("--header-insertion=never")))
;; (use-package lsp-ui
;;   :hook (lsp-mode . lsp-ui-mode)
;;   :custom
;;   (lsp-ui-sideline-show-hover t)
;;   (lsp-ui-doc-enable nil))
;;
;; (use-package lsp-pyright
;;   :hook (python-mode . (lambda()
;;                          (require 'lsp-pyright)
;;                          (lsp))))  ; or lsp-deferred

;;(use-package lsp-treemacs
;;  :after lsp)
;; (add-hook 'python-mode-hook #'lsp)

;;;*** git-gutter
(use-package git-gutter
     :ensure t
     :init (global-git-gutter-mode))
;;;*** expand region
(use-package expand-region)
;  :bind
;  ("C->" . er/expand-region)
;  ("C-<" . er/contract-region))
;;;*** pdf-tools
;; since doc-view is too slow
;; Needed to:
;; dnf install pdf-tools
;; M-x pdf-tools-install
(use-package pdf-tools)
;;;*** popper
(use-package popper
  :ensure t ; or :straight t
  :bind (("C-'"   . popper-toggle-latest)
         ("M-'"   . popper-cycle)
         ("C-M-'" . popper-toggle-type))
  :init
  (setq popper-reference-buffers
        '("\\*Messages\\*"
          "^\\*Warnings\\*"
          "Output\\*$"
          "\\*Async Shell Command\\*"
          "\\*Shell Command Output\\*"
          help-mode
          "^\\*helpful .*\\*"
          completions
          compilation-mode))
  (popper-mode +1)
  (popper-echo-mode +1))                ; For echo area hints
;;;* stupid windows box:
;;(when (getenv "OS")
;;  (when (string-match "Windows.*" (getenv "OS"))
;;    (eval-when-compile (setq locate-command "/home/mobaxterm/bin/mylocate"))
;;    (setq select-enable-clipboard "true")
;;    (set-background-color "black")
;;    (set-foreground-color "white")))

;;;* cycle whitespace-mode

;; these defaults for whitespace settings were snarfed directly from doom:
(defun doom-highlight-non-default-indentation-h () (interactive)
  "Highlight whitespace at odds with `indent-tabs-mode'.
That is, highlight tabs if `indent-tabs-mode' is `nil', and highlight spaces at
the beginnings of lines if `indent-tabs-mode' is `t'. The purpose is to make
incorrect indentation in the current buffer obvious to you.

Does nothing if `whitespace-mode' or `global-whitespace-mode' is already active
or if the current buffer is read-only or not file-visiting."
  (unless (or (eq major-mode 'fundamental-mode)
              (bound-and-true-p global-whitespace-mode)
              (null buffer-file-name))
    (require 'whitespace)
    (set (make-local-variable 'whitespace-style)
         (cl-union (if indent-tabs-mode
                       '(indentation)
                     '(tabs tab-mark))
                   (when whitespace-mode
                     (remq 'face whitespace-active-style))))
    (cl-pushnew 'face whitespace-style) ; must be first
    (whitespace-mode +1)))

;; cycle whitespace-mode between whitespace.el defaults, doom's defaults and off:
;; off=>default=>doom=>off...
(defun bh/toggle-whitespace () (interactive)
       (cond ((string= bh/whitespace-mode "doom")
              (setq bh/whitespace-mode "off")
              (princ "whitespace-mode is off")
              (whitespace-mode -1))
             ((string= bh/whitespace-mode "default")
              (setq bh/whitespace-mode "doom")
              (princ "whitespace-mode is doom's 'highlight non-default indentation'")
              (whitespace-mode -1) ; so that doom-highlight-non-default-indentation-h does something
              (doom-highlight-non-default-indentation-h))
             (t ; (string= bh/whitespace-mode "off")
              (setq whitespace-style (default-value 'whitespace-style)
                    whitespace-display-mappings (default-value 'whitespace-display-mappings)
                    bh/whitespace-mode "default")
              (princ "whitespace-mode is default")
              (whitespace-mode +1))))

(setq tab-always-indent t)
(setq tabify-regexp " [ \t]+")
;;;* Languages
;;;** C and C++ mode stuff

;; this is common for c and c++. Maybe java?

;; From the info page and stroustrup style and edited:
(defconst bh/c-style
  '((c-tab-always-indent        . t)
    (c-comment-only-line-offset . 0)
    (c-hanging-braces-alist     . ((substatement-open before after)
                                   (brace-list-open)))
    (c-hanging-colons-alist     . ((member-init-intro before)
                                   (inher-intro)
                                   (case-label after)
                                   (label after)
                                   (access-label after)))
;;   (c-cleanup-list             . (scope-operator
;;                                  empty-defun-braces
;;                                  defun-close-semi))
    (c-offsets-alist            . ((statement-block-intro . +)
                                   (substatement-open . 0)
                                   (label . 0)
                                   (statement-cont . +)
                                   (case-label . 8)))
;;  (c-offsets-alist            . ((arglist-close . c-lineup-arglist)
;;                                 (substatement-open . 0)
;;                                 (case-label        . 4)
;;                                 (block-open        . 0)
;;                                 (knr-argdecl-intro . -)))
    (c-echo-syntactic-information-p . t)
    (indent-tabs-mode . nil))
  "My C Programming Style" )

(defun bh/c-mode-fiddle ()
  (c-add-style "PERSONAL" bh/c-style t)
  (eval-when-compile (setq c-auto-newline nil)) ; auto-add a newline, yuck!
  (c-toggle-auto-newline -1) ; turn off auto-newline
  (setq indent-tabs-mode nil) ; use spaces for indentation
  (setq tab-width 4)
  (hs-minor-mode) ; hide/show mode - better than outline-mode for C?
  (hs-show-all)
;;  (define-key c-mode-map (kbd "C-<return>") 'c-mark-function)
;;  (define-key c-mode-base-map (kbd "C-m") 'c-context-line-break) ; make RET act like ^J!! and indent
;;  (c-set-offset 'arglist-cont-nonempty 'c-lineup-cont) ; for RSA
  (font-lock-mode)

;(c-set-style "stroustrup")
;(setq c-tab-always-indent nil) ; Only indent if cursor is at left side
;; for auto-newline _and_ hungry-delete:
;; (c-toggle-auto-hungry-state 1)

;;  (setq c-indent-level indent)
;;  (setq c-basic-offset indent)
;;  (setq c-continued-statement-offset indent)
;;  (setq c-brace-imaginary-offset 0)
;;  (setq c-continued-brace-offset (* -1 indent))
;;  (setq c-brace-offset 0)
;;  (setq c-argdecl-indent indent)
;;  (setq c-label-offset (* -1 indent))
  )

;;(setq auto-mode-alist (cons '("\\.C$" . c++-mode) auto-mode-alist))
;;(setq auto-mode-alist (cons '("\\.cc$" . c++-mode) auto-mode-alist))
(add-hook 'c-mode-common-hook 'bh/c-mode-fiddle)

;; This is the example from the info page:
(defun c-lineup-streamop (langelem)
;; lineup stream operators
  (save-excursion
    (let* ((relpos (cdr langelem))
           (curcol (progn (goto-char relpos)
                          (current-column))))
      (re-search-forward "<<\\|>>" (c-point 'eol) 'move)
      (goto-char (match-beginning 0))
      (- (current-column) curcol))))

;(defun c-lineup-cont (langelem)
;;  ;; lineup continuation lines simply by adding the offset - but from
;;  ;; the start of the prior line rather than syntactically - for RSA!!
;;  (save-excursion
;;   (let* ((relpos (cdr langelem))
;;          (curcol (progn (goto-char relpos)
;;                         (current-column))))
;;     (goto-char (c-point 'bol))
;;     (re-search-forward "[^    ]" (c-point 'eol) 'move)
;;     (goto-char (match-beginning 0))
;;     (+ c-basic-offset (- (current-column) curcol)))))

;;
;; Set Stroustrup C/C++/Java coding style
;;
(add-hook 'c-mode-hook
          #'(lambda ()
             (c-set-style "Stroustrup")
             (c-toggle-auto-state -1)
             (setq c-electic-flag nil)))

(add-hook 'c++-mode-hook
          #'(lambda ()
             (c-set-style "Stroustrup")
             (c-toggle-auto-state -1)
             (setq c-electic-flag nil)))

(add-hook 'awk-mode-hook
          #'(lambda ()
             (c-set-style "Stroustrup")
             (c-toggle-auto-state -1)
             (setq c-electic-flag nil)))

(add-hook 'java-mode-hook
          #'(lambda ()
             (c-toggle-auto-state -1)
             (c-set-style "Stroustrup")))

                                        ;-------------------------------------
;;;** shell-script-mode:

(defun bh/sh-indent-rule-for-close-brace ()
  (when (eq ?= (char-before))
    (skip-chars-backward "[:alnum:]_=")
    (current-column)))
;(add-hook 'sh-mode-hook
;;          (lambda ()
;;            (add-hook 'smie-indent-functions
;;                      #'bh/sh-indent-rule-for-close-brace
;;                      nil 'local)))

(defun bh/sh-indent-rules ()
  (setq sh-use-smie nil)
  (setq sh-styles-alist
        '(("bobs"
           (sh-basic-offset . 4)
           (sh-first-lines-indent . 0)
           (sh-indent-after-case . +)
           (sh-indent-after-do . +)
           (sh-indent-after-done . 0)
           (sh-indent-after-else . +)
           (sh-indent-after-if . +)
           (sh-indent-after-loop-construct . +)
           (sh-indent-after-open . +)
           (sh-indent-comment . t)
           (sh-indent-for-case-alt . ++)
           (sh-indent-for-case-label . +)
           (sh-indent-for-continuation . +)
           (sh-indent-for-do . 0)
           (sh-indent-for-done . 0)
           (sh-indent-for-else . 0)
           (sh-indent-for-fi . 0)
           (sh-indent-for-then . 0))))
  (sh-load-style "bobs"))
(add-hook 'sh-mode-hook #'bh/sh-indent-rules)

(defun bh/sh-smie-rules (orig-fun kind token)
  (pcase (cons kind token)
      (`(:before . "|") nil)
    (_ (funcall orig-fun kind token))))
(advice-add 'sh-smie-sh-rules :around #'bh/sh-smie-rules)

(defun bh/sh-mode-customisations ()
  (hs-minor-mode)
  (hs-show-all))
(add-hook 'sh-mode-hook #'bh/sh-mode-customisations)

;;;** nroff etc
;; Set appropriate flags for nroff documents - errors in latest versions
(defun bh/nroff-mode-hook ()
  (setq ispell-filter-hook-args '("-w"))
  (setq ispell-filter-hook "deroff")
  (setq ispell-words-have-boundaries nil)
  (define-key nroff-mode-map (kbd "C-b") #'(lambda ()
                                            (interactive)
                                            (insert "\\fB\\fP")
                                            (backward-char 3)))
  (define-key nroff-mode-map (kbd "C-i") #'(lambda ()
                                            (interactive)
                                            (insert "\\fI\\fP")
                                            (backward-char 3)))
  (define-key nroff-mode-map [f8] 'man-preview))
(add-hook 'nroff-mode-hook 'bh/nroff-mode-hook)
(setq auto-mode-alist (cons '("\\.1$" . nroff-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.1m$" . nroff-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.me$" . nroff-mode) auto-mode-alist))

(autoload 'man-preview "man-preview" nil t)

;;;** python
(add-hook 'python-mode-hook #'bh/python-mode-hook)
(defun bh/python-mode-hook ()
  "the imenu and outline functions in python-mode just don't work for me"
  (interactive)
  (define-key flymake-mode-map (kbd "C-<f7>") #'flymake-goto-next-error)
  (define-key flymake-mode-map (kbd "S-C-<f7>") #'flymake-goto-prev-error)
  (setq-local imenu-generic-expression '((nil "^[ \t]*\\(def\\|class\\) \\(.*\\)$" 2)))
  (setq-local imenu-create-index-function 'imenu-default-create-index-function)
  (setq-local outline-regexp "\\(\\`\\|[ \t]*\\(def\\|class\\) \\|if \\)")
  (setq-local outline-minor-mode-cycle t))

;;;** lisp
; might be fun to play with instead of the one I had in 'local variables'
;; from https://www.emacs.dyerdwelling.family/emacs/20230414111409-emacs--indexing-emacs-init/
(add-hook 'emacs-lisp-mode-hook
 (lambda ()
   (setq imenu-sort-function 'imenu--sort-by-name)
   (setq imenu-case-fold-search t)
  (setq imenu-generic-expression
 '(
	(nil "^;;;[*]+ \\(.*\\)$" 1)
	("defun" "^.*([[:space:]]*defun[[:space:]]+\\([[:word:]-/]+\\)" 1)
	("use-package" "^.*([[:space:]]*use-package[[:space:]]+\\([[:word:]-]+\\)" 1)
	)
 )
  (imenu-add-menubar-index)))
;;;* Useful functions

(defun bh/just-tab ()
  "Just put a fecking tab in, for chrisake!"
  (interactive)
  (insert "\t"))

(defun bh/kill-next-window ()
  "As it says - mainly for when we're full screen"
  (interactive)
  (other-window 1)
  (delete-window))

(defun bh/sudo-find-file (file-name) "Like find file, but opens the file as root."
       (interactive "FSudo Find File: ")
       (let ((tramp-file-name (concat "/sudo::" (expand-file-name file-name))))
         (find-file tramp-file-name)))

(defun bh/paren-match ()
  "Jumps to the paren matching the one under point, and does nothing if there isn't one."
  (interactive)
  (cond ((looking-at "[\(\[{]")
         (forward-sexp 1)
         (backward-char))
        ((looking-at "[])}]")
         (forward-char)
         (backward-sexp 1))
        (t (message "Could not find matching paren."))))

(defun bh/toggle-case-fold ()
  "Toggle the variable case-fold-search."
  (interactive)
  (if (equal case-fold-search nil)
      (progn
        (setq case-fold-search t)
        (message "case-fold-search set to TRUE"))
    (progn
      (setq case-fold-search nil)
      (message "case-fold-search set to nil"))))

(defun bh/toggle-tab-width ()
  "Toggle tab-width between 4 and 8."
  (interactive)
  (if (equal tab-width 4)
      (progn
        (setq tab-width 8)
        (message "tab-width set to 8"))
    (progn
      (setq tab-width 4)
      (message "tab-width set to 4")))
  (recenter-top-bottom))

(defun bh/dup-line () "Duplicate the current line"
       (interactive)
       (beginning-of-line nil)
       (save-excursion
         (bh/yank-whole-line)
         (yank)
         (forward-line -1)))

(defun bh/kill-whole-line () "Delete the current line"
       (interactive)
       (beginning-of-line nil)
       (kill-line 1))

(defun bh/yank-whole-line () "Yank the current line"
       (interactive)
       (save-excursion
         (beginning-of-line nil)
         (let ((beg (point))) (forward-line 1) (kill-ring-save beg (point)))))

(defun bh/copy-line () "Copy the current line to kill buffer"
       (interactive)
       (beginning-of-line nil)
       (set-mark (point))
       (forward-line 1)
       (append-next-kill)
       (copy-region-as-kill (mark) (point)))

(defun bh/time-stamp () "Print a string like 20200830122439"
       (interactive)
       (insert (format-time-string "%Y%m%d%H%M%S")))

(defun bh/date-stamp () "Print s string like Sun Aug 30, 2020 12:25"
       (interactive)
       (insert (format-time-string "%a %b %e, %Y %H:%M ")))

(defun bh/insert-x-selection () ""
       (interactive)
       (insert (gui-get-selection 'PRIMARY 'STRING)))

(defun bh/insert-x-clipboard () ""
       (interactive)
       (insert (gui-get-selection 'CLIPBOARD 'STRING)))

(defun bh/pwd-to-clipboard () ""
       (interactive)
       (setq tmp (pwd))
       (if (string-match "^Directory " tmp)
           (setq tmp (replace-match "" t t tmp)))
       (gui-select-text tmp))

;; from https://emacs.stackexchange.com/questions/46664/switch-between-horizontal-and-vertical-splitting
(defun bh/toggle-window-split ()
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
             (next-win-buffer (window-buffer (next-window)))
             (this-win-edges (window-edges (selected-window)))
             (next-win-edges (window-edges (next-window)))
             (this-win-2nd (not (and (<= (car this-win-edges)
                                         (car next-win-edges))
                                     (<= (cadr this-win-edges)
                                         (cadr next-win-edges)))))
             (splitter
              (if (= (car this-win-edges)
                     (car (window-edges (next-window))))
                  'split-window-horizontally
                'split-window-vertically)))
        (delete-other-windows)
        (let ((first-win (selected-window)))
          (funcall splitter)
          (if this-win-2nd (other-window 1))
          (set-window-buffer (selected-window) this-win-buffer)
          (set-window-buffer (next-window) next-win-buffer)
          (select-window first-win)
          (if this-win-2nd (other-window 1))))))

(defun bh/buffer-menu (&optional arg)
  "runs buffer-menu but with the sense of C-u inverted (ie files only unless C-u is given)"
  (interactive "P")
  (setq arg (not arg))
  (buffer-menu arg))

;; from http://mbork.pl/2014-04-04_Fast_buffer_switching_and_friends
(defun bh/switch-bury-or-kill-buffer (&optional aggr)
  "With no argument, switch (but unlike C-x b, without the need
to confirm).  With C-u, bury current buffer.  With double C-u,
kill it (unless it's modified)."
  (interactive "P")
  (cond
   ((eq aggr nil)
    (progn
      (cl-dolist (buf '("*Buffer List*" "*Ibuffer*" "*Bookmark List* " "*vc-change-log*" "*Locate*" "*grep*" "*compilation*" "*log-edit-files*"))
        (when (get-buffer buf)
          (bury-buffer buf)))
      (switch-to-buffer (other-buffer))))
   ((equal aggr '(4)) (bury-buffer))
   ((equal aggr '(16)) (kill-buffer-if-not-modified (current-buffer)))))
(global-set-key (kbd "C-`") 'bh/switch-bury-or-kill-buffer)

;; from https://www.emacswiki.org/emacs/UnfillParagraph:
    ;;; Stefan Monnier <foo at acm.org>. It is the opposite of fill-paragraph
(defun unfill-paragraph (&optional region)
  "Takes a multi-line paragraph and makes it into a single line of text."
  (interactive (progn (barf-if-buffer-read-only) '(t)))
  (let ((fill-column (point-max))
        ;; This would override `fill-column' if it's an integer.
        (emacs-lisp-docstring-fill-column t))
    (fill-paragraph nil region)))

;; Handy key definition
(define-key global-map "\M-Q" 'unfill-paragraph)

(defun bh/term-at-pwd ()
  "Open a terminal at pwd"
  (interactive)
  (start-process "term" nil "mrxvt"))
;;;** iflipb
(defun bh/iflipb-ignore-buffers (buf)
  "what buffers to ignore in iflipb"
  (cond
   ((equal "*elfeed-search*" buf) nil)
   ((string-match "^[*]" buf) t)
   (:otherwise nil)))
(use-package iflipb
  :config
  (setq-default iflipb-ignore-buffers 'bh/iflipb-ignore-buffers)
  :bind
  ("<C-tab>" . iflipb-next-buffer)
  ("<C-S-iso-lefttab>" . iflipb-previous-buffer))

;;;* keybindings
;;;** Function keys
(global-set-key (kbd "<f1>")            'scroll-other-window)
(global-set-key (kbd "S-<f1>")          #'(lambda () (interactive)
                                           (scroll-other-window '-)))
(global-set-key (kbd "C-<f1>")          'toggle-truncate-lines)
(global-set-key (kbd "<f2>")            'bh/toggle-case-fold)
(global-set-key (kbd "S-<f2>")          'bh/toggle-tab-width)
(global-set-key (kbd "C-<f2>")          'jump-to-register)
(global-set-key (kbd "<f3>")            'ediff-revision)
(global-set-key (kbd "C-<f3>")          'ediff-buffers)
(global-set-key (kbd "S-<f3>")          'magit-status)
(global-set-key (kbd "<f4>")            'bh/term-at-pwd)
(global-set-key (kbd "C-<f4>")          'bh/toggle-whitespace)
(global-set-key (kbd "S-<f4>")          'bh/pwd-to-clipboard)

;(global-set-key (kbd "<f5>")            'rgrep)
;(global-set-key (kbd "<f5>")            'grep-find)
(global-set-key (kbd "<f5>")            'ripgrep-regexp)
(global-set-key (kbd "C-<f5>")          'locate)
(global-set-key (kbd "<f6>")            'bh/kill-whole-line)
(global-set-key (kbd "C-<f6>")          'bh/yank-whole-line)
(global-set-key (kbd "C-S-<f6>")        'bh/dup-line)
(global-set-key (kbd "<f7>")            'compile)
(global-set-key (kbd "S-<f7>")          'kill-compilation)
(global-set-key (kbd "C-<f7>")          'next-error)
(global-set-key (kbd "S-C-<f7>")        #'(lambda () (interactive) (next-error -1)))
(global-set-key (kbd "<f8>")            'bookmark-bmenu-list)
(global-set-key (kbd "C-<f8>")          'ibuffer)
(global-set-key (kbd "S-<f8>")          'bh/buffer-menu)
(global-set-key (kbd "<f9>")            'open-rectangle)
(global-set-key (kbd "S-<f9>")          'ispell-word)
;; NB if <f10> is hard-coded to open the menu, it's a gtk problem. Put '[Settings]\ngtk-menu-bar-accel=' in
;; .config/gtk-3.0/settings.ini
(global-set-key (kbd "<f10>")           'kill-rectangle)
(global-set-key (kbd "S-<f10>")         'copy-rectangle-as-kill)
(global-set-key (kbd "C-<f10>")         'recentf-open-files)
(global-set-key (kbd "<f11>")           'yank-rectangle)
(global-set-key (kbd "C-<f11>")         'bh/date-stamp)
(global-set-key (kbd "S-<f11>")         'bh/time-stamp)
;; (global-set-key (kbd "M-<f11>")       nil) let window mgr have this for volume-down on achar
;; (global-set-key (kbd "S-<f12>")         'UNASSIGNED)
(global-set-key (kbd "<f12>")           'other-window)
;; (global-set-key (kbd "M-<f12>")       nil) let window mgr have this for volume-up on achar

;;;*** gdb stuff:
                                        ; maybe use gud-mode-hook?
                                        ;(global-set-key (kbd "M-<f1>")          'gud-next) ; C-c C-n
(global-set-key (kbd "M-<f1>")          'gdb-restore-windows)
                                        ;(global-set-key (kbd "M-<f2>")          'gud-step) ; C-c C-s
(global-set-key (kbd "M-<f2>")          'gdb-many-windows)
                                        ;(global-set-key (kbd "M-<f3>")          'gud-cont) ; C-c C-r
                                        ;(global-set-key (kbd "M-<f4>")          'gud-break); C-c C-b (C-c C-d to delete breakpoint)
                                        ;(global-set-key (kbd "M-<f5>")          'gud-run)
                                        ;(global-set-key (kbd "M-<f6>")          'gud-tbreak)
(global-set-key (kbd "M-<f7>")          'gud-up)
(global-set-key (kbd "M-<f8>")          'gud-down)
                                        ;(global-set-key (kbd "M-<f9>")          'gud-until)
                                        ;(global-set-key (kbd "M-<f10>")         'gud-finish)
                                        ;(global-set-key (kbd "M-<f11>")         #'(lambda()
                                        ;                                            "Set temporary break and go"
                                        ;                                            (interactive)
                                        ;                                            (gud-tbreak t)
                                        ;                                            (gud-cont nil)))
                                        ; (global-set-key (kbd "M-<f12>")         'gud-print)

;; ** C-x keybinds
(define-key ctl-x-map "%"                'bh/paren-match)
(define-key ctl-x-map "="                'what-line)

(global-set-key (kbd "C-x C-b")        'bh/buffer-menu)

(define-key ctl-x-map "g"                'goto-line)
(define-key ctl-x-map "m"                #'(lambda  ()
                                            "Print manual entry for word at point"
                                            (interactive)
                                            (manual-entry (current-word))))
(define-key ctl-x-map "/"                'query-replace-regexp)
(define-key ctl-x-map "C-v"              'vc-next-action)
(define-key ctl-x-map "C-w"              'write-file) ; ido-write-file is stupid: in fact, need to use C-x C-w C-w

;;;** Misc keybinds
(global-set-key (kbd "M-<delete>")      'flush-lines)
(global-set-key (kbd "C-<delete>")      'keep-lines)
(global-set-key (kbd "M-s")             'isearch-forward-regexp)

;; (global-set-key (kbd "S-<iso-lefttab>") 'bh/just-tab) org mode uses this

;; leave these alone - default mapping extends the selection when shifted
;(global-set-key (kbd "M-f")             'forward-whitespace)
;(global-set-key (kbd "M-b")             #'(lambda ()
;                                           (interactive) (forward-whitespace '-1)))
;(global-set-key (kbd "C-<left>")        'backward-word)
;(global-set-key (kbd "C-<right>")       'forward-word)
;(global-set-key (kbd "C-S-<right>")     'forward-whitespace)
;(global-set-key (kbd "C-S-<left>")      #'(lambda ()
;                                           (interactive) (forward-whitespace '-1)))

(global-set-key (kbd "S-<insert>")      'bh/insert-x-selection)
(global-set-key (kbd "C-S-v")           'bh/insert-x-clipboard)
(global-set-key (kbd "C-v")             'bh/insert-x-clipboard)

(global-set-key [remap dabbrev-expand] 'hippie-expand) ; M-/
;; (global-set-key (kbd "C-\\")            'hippie-expand) ; shadows use in company-try-hard
(append hippie-expand-try-functions-list
        `(try-expand-dabbrev-visible
          (lambda (arg) (call-interactively 'company-complete))))

(global-set-key (kbd "C-z")             'undo)
(global-set-key (kbd "C-|")             'repeat-complex-command)
(global-set-key (kbd "M-;")             'bh/toggle-window-split) ; unshifted M-: !!
(global-set-key (kbd "C-<return>")      'mark-defun)

;; something (what? - dired+ !!! but also ido-menu!!!) stole these:
(global-set-key (kbd "C-t")             'transpose-chars)
(global-set-key (kbd "M-t")             'transpose-words)

;; Muscle memory fix - I always forget to use 'e' in buffer-menu and the default
;; pushes the selected buffer into the 'next' window - very annoying
(define-key Buffer-menu-mode-map (kbd "<return>") 'Buffer-menu-this-window)

;; zoom in and out:
(global-set-key (kbd "C-+")                 'text-scale-adjust)
(global-set-key (kbd "C--")                 'text-scale-adjust)
(global-set-key (kbd "C-_")                 'text-scale-decrease)
(global-set-key (kbd "C-=")                 'text-scale-increase)
(global-set-key (kbd "C-0")                 'text-scale-adjust)
;; because scroll-right/left are not very useful and I keep hitting C-<next> by mistake
(global-set-key (kbd "C-<next>")            #'(lambda () (interactive) (scroll-down 1)))
(global-set-key (kbd "C-<prior>")           #'(lambda () (interactive) (scroll-up 1)))
(global-set-key (kbd "M-\"")                #'(lambda () (interactive) (insert "# shellcheck disable=SC2034")))

;;;* scroll on mouse wheel

(defun bh/up-slightly ()   (interactive) (scroll-up 5))
(defun bh/down-slightly () (interactive) (scroll-down 5))
(defun bh/up-one ()        (interactive) (scroll-up 1))
(defun bh/down-one ()      (interactive) (scroll-down 1))
(defun bh/up-a-lot ()      (interactive) (scroll-up))
(defun bh/down-a-lot ()    (interactive) (scroll-down))

(global-set-key  (kbd "S-<mouse4>") 'bh/down-one)
(global-set-key  (kbd "S-<mouse5>") 'bh/up-one)
(global-set-key  (kbd "<mouse4>")   'bh/down-slightly)
(global-set-key  (kbd "<mouse5>")   'bh/up-slightly)
(global-set-key  (kbd "C-<mouse4>") 'bh/down-a-lot)
(global-set-key  (kbd "C-<mouse5>") 'bh/up-a-lot)

;; emacs-pgtk
;(global-set-key  (kbd "<wheel-down>")   'scroll-bar-toolkit-scroll)
;(global-set-key  (kbd "<wheel-up>")   'bh/up-slightly)

;;;* GJOTS MODE
;; remaining problems:
;; you must show-all before saving
;; it prompts for coding. default raw-text is OK
(setq format-alist
      (cons '(gjots "gjots" nil "gjots2org" "org2gjots" t nil) format-alist))
(define-derived-mode gjots-mode org-mode "gjots"
  "Major mode for editing gjots files."
  (format-decode-buffer 'gjots)
  (outline-hide-sublevels '1))
;(autoload 'gjots-mode "gjots-mode" "Major mode to edit gjots files." t)
;(setq auto-mode-alist
;;      (cons '("\\.gjots$" . gjots-mode) auto-mode-alist))

;;;* Syntax checking on file save:
(defun bh/check-syntax ()
  "Check syntax for various languages."
  (interactive)
  (when (eq major-mode 'lisp-mode)
    (progn
      (emacs-lisp-byte-compile)))
  (when (eq major-mode 'sh-mode)
    (compile (format "bash -n %s && shellcheck -f gcc %s" buffer-file-name buffer-file-name) t))
  (when (eq major-mode 'ruby-mode)
    (compile (format "ruby -c %s" buffer-file-name) t))
  (when (eq major-mode 'python-mode)
    (compile (format "python -B -m py_compile %s" buffer-file-name) t))
  (when (eq major-mode 'awk-mode)
    (compile (format "AWKPATH=$PATH gawk --lint --source 'BEGIN { exit(0) } END { exit(0) }' --file %s" buffer-file-name) t)))

(setq compilation-error-regexp-alist (append '(shellcheck-mine) compilation-error-regexp-alist))
(setq compilation-error-regexp-alist-alist (append  '((shellcheck-mine
                                                      "^\\([^:]+\\):\\([0-9]+\\):\\([0-9]+\\): .*"
                                                      1 2 3)) compilation-error-regexp-alist-alist))

;; This stops compile window from opening if there are no errors.
;; from https://stackoverflow.com/questions/8309769/how-can-i-prevent-emacs-from-opening-new-window-for-compilation-output
(defun compilation-exit-autoclose (STATUS code msg)
  "Close the compilation window if there was no error at all."
  ;; If M-x compile exists with a 0
  (when (and (eq STATUS 'exit) (zerop code))
    ;; then bury the *compilation* buffer, so that C-x b doesn't go there
    (bury-buffer)
    ;; and delete the *compilation* window
    (delete-window (get-buffer-window (get-buffer "*compilation*"))))
  ;; Always return the anticipated result of compilation-exit-message-function
  (cons msg code))
(setq compilation-exit-message-function 'compilation-exit-autoclose)

(add-hook 'after-save-hook #'bh/check-syntax)

;;;* menu-bar
(setq yank-menu-length 60)
(menu-bar-mode)
;(use-package 'easymenu) ; https://www.emacswiki.org/emacs/EasyMenu - built-in
(easy-menu-define my-menu global-map "My own menu"
  '("My Stuff"
    ["browse-kill-ring" browse-kill-ring t]
    ["Spell word" ispell-word]
    ["Date stamp 'Fri Sep 17, 2021 11:46'" bh/date-stamp]
    ["Time stamp '20210917114946'" bh/time-stamp]
    ["Recent files" recentf-open-files]
    ["Delete matching lines" flush-lines]
    ["Keep matching lines" keep-lines]
    ["Toggle h/v split" bh/toggle-window-split]
    ["Expand region" er/expand-region]
    ["Shrink region" er/contract-region]
    ["Goto address at point C-c Ret" goto-address-at-point]
    ["Terminal at cwd" bh/term-at-pwd]
    ["Unformat para" unfill-paragraph]
    ["Bookmark file" bmkp-bookmark-set-confirm-overwrite]
    ["Org breadcrumbs" (org-display-outline-path nil t)]
    ("Mark"
     ["word" mark-word]
     ["sentence" er/mark-sentence]
     ["para" mark-paragraph]
     ["sexp" mark-sexp]
     ["defun" mark-defun]
     ["to end" mark-end-of-buffer]
     ["url" er/mark-url]
     ["email" er/mark-email])))
;; (easy-menu-add-item nil '("tools") ["IRC" erc-select t])

;;;* Misc

;; fix for PureGTK emacs after 20210814:
(unless (fboundp 'x-select-font)
  (defalias 'x-select-font 'pgtk-popup-font-panel "Pop up the font panel.
This function has been overloaded in Nextstep."))

;; see if this helps:
(setq help-window-select t)

(setq find-file-visit-truename t) ;;; follow sym links
(global-auto-revert-mode)

;-------------------------------------
;; Preserve links to files during backup
(setq-default backup-by-copying-when-linked t)

;-------------------------------------
;; Other Emacs variables ...
(column-number-mode 1)

(fset 'yes-or-no-p #'y-or-n-p)           ;replace y-e-s by y
;(defalias 'yes-or-no-p 'y-or-n-p)

;; Always end a file with a newline
;;(setq require-final-newline t)
;(setq mode-require-final-newline nil)

;; Stop at the end of the file, not just add lines
;(setq next-line-add-newlines nil)

;; Always spaces, never TABs
(setq-default indent-tabs-mode nil)
;(setq default-tab-width 4)

;; don't popup ediff frames:
(setq ediff-window-setup-function #'ediff-setup-windows-plain)

(setq frame-title '("emacs: %b %& %f"))

(setq gdb-many-windows t)

(make-variable-buffer-local 'compile-command)
(set-default 'compile-command "make -k")

(setq show-paren-style 'expression)
(show-paren-mode)

(server-start)

(delete-selection-mode t)
(setq sentence-end "[.?!][]\"')}]*\\($\\|[ \t]\\)[ \t\n]*")
(setq sentence-end-double-space nil)
(set-scroll-bar-mode 'right)
(tool-bar-mode -1) ; no tool bar, thanks
(transient-mark-mode t)
;;   (load-file "~/.config/emacs/jkb-compr-ccrypt.el")))
;;   (load-file "/usr/share/doc/ccrypt-1.2/jka-compr-ccrypt.el")
;;   (require 'jka-compr-ccrypt "jka-compr-ccrypt.el")))

; error in latest version:
;(require 'ps-ccrypt "ps-ccrypt.el") ; this only exists in .config/emacs so can't use-package it

;; this is built-in: but it works badly with most themes:
(setq global-hl-line-sticky-flag t)
(global-hl-line-mode)
(blink-cursor-mode)

;; take off training wheels:
(put 'narrow-to-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; Make Text mode the default mode for new buffers.
(setq default-major-mode 'text-mode)

;; Turn on Auto Fill mode automatically in Text mode and related modes.
(add-hook 'text-mode-hook
          (lambda ()
            (auto-fill-mode 1)))

;; I think RedHat puts a symbolic link in, Gentoo needs this:
;; doom has hunspell ... seems OK
(setq-default ispell-program-name "aspell")

;; (setq mouse-yank-at-point t) ; nooooooooo!

;;(windmove-default-keybindings)

;; Don't use dialog boxes - Clicking on an install button for instance
;; makes Emacs spawn dialog boxes from that point on:
(setq use-dialog-box nil)

(setq makefile-warn-suspicious-lines-p nil)

;(autoload 'new-doc (expand-file-name "~/.config/emacs/new-doc") "Create a new fax.
;Use prefix argument to send a letter instead." t)
;(autoload 'ispell-word "ispell" "Check spelling of word at or before point" t)
;(autoload 'ispell-complete-word "ispell" "Complete word at or before point" t)
;(autoload 'ispell-region "ispell" "Check spelling of every word in the region" t)
;(autoload 'ispell-buffer "ispell" "Check spelling of every word in the buffer" t)
(autoload 'calculate-rectangle "calc-rect" "Calculate sum of numbers in rectangle" t)
;(autoload 'calc             "calc.elc"     "Calculator Mode" t nil)
;(autoload 'calc-extensions  "calc-ext.elc" nil nil nil)
;(autoload 'quick-calc       "calc.elc"     "Quick Calculator" t nil)
;(autoload 'calc-grab-region "calc-ext.elc" nil t nil)
;(autoload 'defmath          "calc-ext.elc" nil t t)
;(autoload 'edit-kbd-macro      "macedit.elc" "Edit Keyboard Macro" t nil)
;(autoload 'edit-last-kbd-macro "macedit.elc" "Edit Keyboard Macro" t nil)
;; (autoload 'c++-mode "c++-mode" "Mode for editing c++" t nil)
;(autoload 'c++-mode  "cc-mode" "C++ Editing Mode" t)
;(autoload 'c-mode    "cc-mode" "C Editing Mode" t)
;(autoload 'objc-mode "cc-mode" "Objective-C Editing Mode" t)
;(autoload 'sgml-mode "sgml-mode" "Major mode to edit SGML files." t)
;(autoload 'xml-mode "sgml-mode" "Major mode to edit XML files." t)
;(autoload 'puppet-mode "puppet-mode" "Major mode to edit puppet files." t)
(setq auto-mode-alist
      (append '(
                ("\\.C$"   . c++-mode)
                ("\\.cpp$" . c++-mode)
                ("\\.cc$"  . c++-mode)
                ("\\.c$"   . c-mode)
                ("\\.h$"   . c-mode)
                ("\\.pp$"  . puppet-mode)
                ("\\.m$"   . objc-mode)) auto-mode-alist))

;; Calculator
(global-set-key (kbd "M-#") 'calc)

(setq mouse-autoselect-window t)
(pixel-scroll-precision-mode t) ;; fails on defvar-1 with emacs-28
(global-goto-address-mode)

;; fix multibyte chars in elfeed - or run toggle-enable-multibyte-characters twice!
(set-language-environment "utf-8")
;;;** electric-indent-mode
;; I don't like electric-indent-mode auto-indenting in org-mode. This seems to be
;; a solution: https://www.philnewton.net/blog/electric-indent-with-org-mode/
(add-hook 'electric-indent-functions
          (lambda (x) (when (eq 'org-mode major-mode) 'no-indent)))
;;;* backup - don't use ~ files! from ohai:

;; Emacs writes backup files to `filename~` by default. This is messy,
;; so let's tell it to write them to `~/.emacs.d/bak` instead.
;; If you have an accident, check this directory - you might get lucky.
(setq backup-directory-alist
      `(("." . ,(expand-file-name (concat dotfiles-dir "bak")))))
;;;* dired
;; I fail to see anything of value in dired+:
;; https://www.emacswiki.org/emacs/DiredPlus
;; update with:
;; cd ~/dotfiles/.config/emacs/dired+
;; wget -N https://www.emacswiki.org/emacs/download/dired+.el
;; from http://mbork.pl/2015-04-25_Some_Dired_goodies
;; (setq load-path (append load-path '("~/.config/emacs/dired+")))
;; (require 'dired+)
;;(require 'help-fns+) ... no. I use helpful!

;; let's revisit these:
;;(put 'dired-find-alternate-file 'disabled nil) ; visiting a file from dired closes the dired buffer
;; dired - For the few times I’m using Dired, I prefer it not spawning
;; an endless amount of buffers. In fact, I’d prefer it using one
;; buffer unless another one is explicitly created, but you can’t have
;; everything:
;;(with-eval-after-load 'dired
;;  (define-key dired-mode-map (kbd "RET") 'dired-find-alternate-file))
;;;* psvn fix
;; https://www.emacswiki.org/emacs/SvnStatusMode:
(add-hook 'svn-pre-parse-status-hook 'bh/svn-status-parse-fixup-externals-full-path)
(defun bh/svn-status-parse-fixup-externals-full-path ()
  "Subversion 1.7 adds the full path to externals.  This
pre-parse hook fixes it up to look like pre-1.7, allowing
psvn to continue functioning as normal."
  (goto-char (point-min))
  (let (( search-string  (file-truename default-directory) ))
    (save-match-data
      (save-excursion
        (while (re-search-forward search-string (point-max) t)
          (replace-match "" nil nil))))))

;;;* SRC stuff:
(defvar bh/checkin-on-save t)
(defun bh/ensure-in-vc-or-check-in ()
  "Automatically checkin file if it's under the control of SRC.
<file>,v can be in the same directory or in the subdirectory RCS
or .src. The idea is to call vc-checkin only for files not for
buffers."
  (interactive)
  (when (and (bound-and-true-p bh/checkin-on-save) buffer-file-name)
    (setq backend (ignore-errors (vc-responsible-backend (buffer-file-name))))
    (if (bound-and-true-p backend)
        (when (string= backend 'SRC)
          ; why does this insist on popping up a window containing the filename?
          ;(setq log-edit-confirm nil) ; doesn't help
          (vc-checkin (list buffer-file-name) backend "auto-checkin" nil)
          ; throws error on nil: (delete-window (get-buffer-window log-edit-files-buf))
      (vc-register)))))

;;;* themes
(add-to-list 'custom-theme-load-path "~/.config/emacs/themes")
; (load-theme 'dichromacy-bh t) ; 't' means no check
;; modus-themes are built-in now so no need for require/use-package
;(use-package modus-themes
;  :ensure
;  :init
;  ;; Add all your customizations prior to loading the themes
;  (setq modus-themes-italic-constructs t
;        modus-themes-syntax '(yellow-comments)
;        modus-themes-completions '((selection . (intense))
;                                   (matches . (background intense))))
;
;  ;; Load the theme files before enabling a theme (else you get an error).
;  (modus-themes-load-themes)
;
;  :config
;  ;; Load the theme of your choice:
;  (mapcar 'disable-theme custom-enabled-themes)
;  (if (eq
;       "dark\n"
;       (with-temp-buffer
;         (insert-file-contents "~/.cache/dark-mode")
;         (buffer-string)))
;      (modus-themes-load-vivendi)
;    (modus-themes-load-operandi)))

(mapcar 'disable-theme custom-enabled-themes)
(if (string=
     "dark\n"
     (with-temp-buffer
       (insert-file-contents "~/.cache/dark-mode")
       (buffer-string)))
    (load-theme 'modus-vivendi)
  (load-theme 'modus-operandi))

;;;* modeline
(use-package all-the-icons :defer t)

(use-package doom-modeline
  ;; :defer t ; faster without this!
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15)))

;; add the breadcrumbs in org mode:
;; https://emacs.stackexchange.com/questions/30894/show-current-org-mode-outline-position-in-modeline
(defun org-which-function ()
  (interactive)
  (when (eq major-mode 'org-mode)
    (mapconcat 'identity (org-get-outline-path t)
               ">")))
(which-function-mode 1)
;;;* timestamp messages
;; https://emacs.stackexchange.com/questions/32150/how-to-add-a-timestamp-to-each-entry-in-emacs-messages-buffer
(defun sh/current-time-microseconds ()
  "Return the current time formatted to include microseconds."
  (let* ((nowtime (current-time))
         (now-ms (nth 2 nowtime)))
    (concat (format-time-string "[%Y-%m-%dT%T" nowtime) (format ".%d]" now-ms))))

(defun sh/ad-timestamp-message (FORMAT-STRING &rest args)
  "Advice to run before `message' that prepends a timestamp to each message.

Activate this advice with:
(advice-add 'message :before 'sh/ad-timestamp-message)"
  (unless (string-equal FORMAT-STRING "%s%s")
    (let ((deactivate-mark nil)
          (inhibit-read-only t))
      (with-current-buffer "*Messages*"
        (goto-char (point-max))
        (if (not (bolp))
            (newline))
        (insert (sh/current-time-microseconds) " ")))))

(advice-add 'message :before 'sh/ad-timestamp-message)
;;;* Final

;; paradox is slow to load and package.el is good enough for the above
;; paradox goes into an infinite 100% cpu loop under puregtk+gccemacs
;(use-package paradox :defer t)
;(setq paradox-github-token '7a71b1bbede3b318a37e875efcb404035da735c6)

;; recentf list only gets saved on graceful exit - so it does not get
;; saved on abnormal exit or when running emacs server. But during
;; startup a number of files gets visited, whether you have
;; desktop-save-mode or not and it slows startup a lot. So delay adding
;; the hook to the last possible moment.
(defun bh/recentf-mode-startup ()
  (recentf-mode 1)
  (setq recentf-max-saved-items nil) ; infinite - manage manually?
  (add-hook 'find-file-hook #'recentf-save-list))
(add-hook 'desktop-after-read-hook #'bh/recentf-mode-startup)

;;;* Local variables

;; NB org mode has this ((nil "^\\(?:[*\f]+\\).*$" 0))
;; playing with it ...
;;; (setq imenu-generic-expression '((nil "^;;;\\(?:[*\f]+\\) \\(.*\\)$" 2)))
;;; (setq imenu-generic-expression '((nil "^;;;\\*+ \\(.*\\)$" 1)))

;; ChatGPT gave me this which kinda sorta works, but not really.
;;(setq imenu-generic-expression
;;      '(("Sections" "^;;;\\* \\(.+\\)$" 1)
;;        ("Subsections" "^;;;\\*\\* \\(.+\\)$" 1)
;;        ("Subsubsections" "^;;;\\*\\*\\* \\(.+\\)$" 1)))

;; (setq-local imenu-case-fold-search t) ; doesn't work?

;; this works but wtf is |` about?
;; (setq-local outline-regexp ";;;\\*+\\|\\`")

;; NB in outline-regex, ` is a puzzle but it doesn't work without it!!:
;; ‘\`’
;;     matches the empty string, but only at the beginning of the string
;;     or buffer (or its accessible portion) being matched against.

;; (setq-local outline-regexp ";;;\\(;* [^ \t\n]\\)")

;; from lisp-mode:
;;(setq outline-regexp (concat ";;;;* [^ \t\n]\\|(\\|\\(" lisp-mode-autoload-regexp "\\)"))
;;(setq-local outline-regexp (concat ";;;[*]+ [^ \t\n]\\|(\\|\\(" lisp-mode-autoload-regexp "\\)"))

;; it seems these imenu settings are not needed to get speedbar working:
;; eval: (setq imenu-sort-function 'imenu--sort-by-name)

;; imenu-generic-expression: ((nil "^;;;[*]+ \\(.*\\)$" 1)) - the list hook (in languages->lisp) wins

;; Local Variables:
;; outline-regexp: ";;;\\*+\\|\\`"
;; eval: (outline-minor-mode 1)
;; eval: (outline-hide-sublevels 4)
;; End:

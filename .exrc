" Needed for broken xterm terminfo's eg solaris:
map [A k
map [B j
map [C l
map [D h
map! ^[[A ^[ka
map! ^[[B ^[ja
map! ^[[C ^[la
map! ^[[D ^[ha
"
map [1~ 0
map [7~ 0
map [4~ $
map [8~ $
map [5~ 
map [6~ 
map Od B
map Oc W
map Ob }
map Oa {
"
" Nice macro to reformat lines:
map  !}fmt
" * 'next'
map  :wn
"
map! Od Bi
map! Oc Wi
map! Ob }a
map! Oa {i
"
set ignorecase
set nowrapscan

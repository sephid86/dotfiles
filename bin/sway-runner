#!/usr/bin/env bash
# https://gitlab.com/wef/dotfiles/-/blob/master/bin/sway-join
# shellcheck disable=SC2034
TIME_STAMP="20230410.100515"

# Copyright (C) 2023 Bob Hepple <bob.hepple@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

PROG=$(basename "$0")
HEADLESS_CONFIG="${XDG_CONFIG_DIR:-$HOME/.config}/sway/headless"
USAGE="Run a swaymsg(1) command on the Nth sway session, typically from a
ssh tty or from cron(1) or at(1). Obviously, in a sway session, you'd
just run the swaymsg(1) command from a terminal.

If no sway session is running then a headless session will be started.

When running a new headless session, it is assumed that there is a
configuration file for it at $HEADLESS_CONFIG
(or use the -c option).

eg to start wayvnc on an existing sway session from a ssh session (ie
not from the sway session itself):

\$ $PROG exec wayvnc

eg to control a sway session from an ssh tty logged in as another user:
\$ sudo su - <sway-user> -- $PROG 'workspace number 3'

eg to run xeyes on a an existing sway session on a remote system:
\$ ssh <sway-user>@<remote-system> 'export PATH=\$PATH:\$HOME/bin; $PROG -- exec xeyes'

eg to run xclock at 6pm:
\$ echo '$PROG exec xclock' | at 18:00


Pre-requisite: argp.sh from the same place you found this.
"

ARGS="
ARGP_DELETE=quiet
ARGP_VERSION=$VERSION
ARGP_PROG=$PROG
##############################################################   
#OPTIONS:
#name=default             sname arg       type range description
##############################################################
NTH='0'                   n     n         i    0-    ordinal of the sway session.
NEW=''                    N     ''        b    ''    don't look for an existing session - create a new one.
LIST=''                   l     ''        b    ''    list sway sessions
KILL_SESSION=''           K     ''        b    ''    kill the Nth sway session (careful! no confirmation is offered)
CONFIG='$HEADLESS_CONFIG' c     config    s    ''    sway config to use when starting a new headless session.
##############################################################   
ARGP_ARGS=command
ARGP_SHORT=$SHORT_DESC
ARGP_USAGE=$USAGE"

exec 4>&1
eval "$(echo "$ARGS" | argp.sh "$@" 3>&1 1>&4 || echo exit $? )"
exec 4>&-

[[ "$NTH" =~ ^[0-9]+$ ]] || NTH="0"

ID=$( id -u )

# shellcheck disable=SC2029,SC2087,SC2086
get_sock_from_user_and_sway_pid() {
    ID="$1"
    SWAY_PID="$2"
    echo "${XDG_RUNTIME_DIR:-/run/user/$ID}/sway-ipc.$ID.$SWAY_PID.sock"
}

get_env_param_from_pid() {
    PID="$1"
    PARAM="$2"
    [[ "$PID" ]] && tr "\0" "\n" < "/proc/$PID/environ" | awk -F '=' "/^$PARAM/"' {print $2}'
}

get_pids_of() {
    ID="$1"
    prog="$2"
    pgrep --euid "$ID" --exact "$prog"
}

get_pid_for_program_on_swaysock() {
    ID="$1"
    SOCK="$2"
    PROGRAM="$3"
    for PID in $( get_pids_of "$ID" "$PROGRAM"); do
        S=$( get_env_param_from_pid "$PID" "SWAYSOCK" )
        case $S in
            "$SOCK")
                echo "$PID"
                return
                ;;
            *)
                ;;
        esac
    done
}

[[ "$VERBOSE" ]] && set -x

[[ "$LIST" ]] && {
    n=0
    for SWAY_PID in $( get_pids_of "$ID" sway ); do
        SOCK=$( get_sock_from_user_and_sway_pid "$ID" "$SWAY_PID" )
        echo "$n: $SWAY_PID: $SOCK"
        n=$(( n + 1 ))
    done
    exit 0
}

# look for an existing sway session corresponding to NTH
N=0
SWAY_PID=
for PID in $( get_pids_of "$ID" sway ); do
    if (( N == NTH )); then
        SWAY_PID="$PID"
        break
    fi
    N=$(( N + 1 ))
done

[[ "$KILL_SESSION" ]] && {
    if [[ "$SWAY_PID" ]]; then
        kill "$SWAY_PID"
        exit 0
    else
        echo "$PROG: no such session #$NTH" >&2
        exit 1
    fi
}

(( $# > 0 )) || {
    echo "$PROG: no command given; doing nothing" >&2
    exit 1
}

[[ -z "$SWAY_PID" || "$NEW" ]] && {
    # start a new session:
    export WLR_BACKENDS=headless 
    export WLR_LIBINPUT_NO_DEVICES=1 
    sway --config "$HEADLESS_CONFIG" > ~/.wsession-errors.headless 2>&1 & SWAY_PID=$!
    sleep 3 # to give /run/user/$ID/wayland-? time to be created
}

[[ "$SWAY_PID" ]] && {
    [[ "$SOCK" ]] || SOCK=$( get_sock_from_user_and_sway_pid "$ID" "$SWAY_PID" )

    swaymsg -q -s "$SOCK" -- "$*"
}

# Local Variables:
# mode: shell-script
# time-stamp-pattern: "4/TIME_STAMP=\"%:y%02m%02d.%02H%02M%02S\""
# eval: (add-hook 'before-save-hook 'time-stamp)
# End:
